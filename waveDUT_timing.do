onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb4countertime/ValLd_s
add wave -noupdate /tb4countertime/up_s
add wave -noupdate /tb4countertime/down_s
add wave -noupdate /tb4countertime/nLd_s
add wave -noupdate /tb4countertime/clk_s
add wave -noupdate /tb4countertime/nres_s
add wave -noupdate -divider -height 50 atop
add wave -noupdate /tb4countertime/did_atop_s
add wave -noupdate /tb4countertime/err_atop_s
add wave -noupdate /tb4countertime/cnt_atop_s
add wave -noupdate -divider -height 50 atgl
add wave -noupdate /tb4countertime/did_atgl_s
add wave -noupdate /tb4countertime/err_atgl_s
add wave -noupdate /tb4countertime/cnt_atgl_s
add wave -noupdate -divider -height 50 atil
add wave -noupdate /tb4countertime/did_atil_s
add wave -noupdate /tb4countertime/err_atil_s
add wave -noupdate /tb4countertime/cnt_atil_s
add wave -noupdate -divider -height 50 ok
add wave -noupdate /tb4countertime/ok_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {159230 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 227
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1697940 ps}
