--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: DUT_timesim.vhd
-- /___/   /\     Timestamp: Fri Dec 11 13:08:32 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm DUT -w -dir netgen/fit -ofmt vhdl -sim DUT.nga DUT_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: DUT.nga
-- Output file	: D:\DTP\dtp_a3\dt_aufgabe3\ise_14x4\iseWRK\netgen\fit\DUT_timesim.vhd
-- # of Entities	: 1
-- Design Name	: DUT.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity DUT is
  port (
    clk : in STD_LOGIC := 'X'; 
    nres : in STD_LOGIC := 'X'; 
    down : in STD_LOGIC := 'X'; 
    nLd : in STD_LOGIC := 'X'; 
    up : in STD_LOGIC := 'X'; 
    err : out STD_LOGIC; 
    valLd : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    cnt : out STD_LOGIC_VECTOR ( 11 downto 0 ); 
    did : out STD_LOGIC_VECTOR ( 2 downto 0 ) 
  );
end DUT;

architecture Structure_atop of DUT is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal down_II_UIM_5 : STD_LOGIC; 
  signal nLd_II_UIM_7 : STD_LOGIC; 
  signal valLd_0_II_UIM_9 : STD_LOGIC; 
  signal up_II_UIM_11 : STD_LOGIC; 
  signal valLd_10_II_UIM_13 : STD_LOGIC; 
  signal valLd_3_II_UIM_15 : STD_LOGIC; 
  signal valLd_1_II_UIM_17 : STD_LOGIC; 
  signal valLd_2_II_UIM_19 : STD_LOGIC; 
  signal valLd_5_II_UIM_21 : STD_LOGIC; 
  signal valLd_4_II_UIM_23 : STD_LOGIC; 
  signal valLd_8_II_UIM_25 : STD_LOGIC; 
  signal valLd_6_II_UIM_27 : STD_LOGIC; 
  signal valLd_7_II_UIM_29 : STD_LOGIC; 
  signal valLd_9_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal cnt_0_MC_Q_35 : STD_LOGIC; 
  signal cnt_10_MC_Q_37 : STD_LOGIC; 
  signal cnt_11_MC_Q_39 : STD_LOGIC; 
  signal cnt_1_MC_Q_41 : STD_LOGIC; 
  signal cnt_2_MC_Q_43 : STD_LOGIC; 
  signal cnt_3_MC_Q_45 : STD_LOGIC; 
  signal cnt_4_MC_Q_47 : STD_LOGIC; 
  signal cnt_5_MC_Q_49 : STD_LOGIC; 
  signal cnt_6_MC_Q_51 : STD_LOGIC; 
  signal cnt_7_MC_Q_53 : STD_LOGIC; 
  signal cnt_8_MC_Q_55 : STD_LOGIC; 
  signal cnt_9_MC_Q_57 : STD_LOGIC; 
  signal did_0_MC_Q_59 : STD_LOGIC; 
  signal did_1_MC_Q_61 : STD_LOGIC; 
  signal did_2_MC_Q_63 : STD_LOGIC; 
  signal err_MC_Q_65 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_D_67 : STD_LOGIC; 
  signal Gnd_68 : STD_LOGIC; 
  signal Vcc_69 : STD_LOGIC; 
  signal cnt_0_MC_D1_70 : STD_LOGIC; 
  signal cnt_0_MC_D2_71 : STD_LOGIC; 
  signal cnt_1_down_cs_72 : STD_LOGIC; 
  signal N_PZ_168_73 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_74 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_75 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_Q : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D_77 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D1_78 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D2_79 : STD_LOGIC; 
  signal N_PZ_168_MC_Q_80 : STD_LOGIC; 
  signal N_PZ_168_MC_D_81 : STD_LOGIC; 
  signal N_PZ_168_MC_D1_82 : STD_LOGIC; 
  signal N_PZ_168_MC_D2_83 : STD_LOGIC; 
  signal cnt_1_up_cs_85 : STD_LOGIC; 
  signal N_PZ_168_MC_D2_PT_0_86 : STD_LOGIC; 
  signal N_PZ_168_MC_D2_PT_1_87 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D_89 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D1_90 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_91 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_0_92 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_1_93 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_2_94 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_Q : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D_96 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D1_97 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D2_98 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_10_MC_D_100 : STD_LOGIC; 
  signal cnt_10_MC_D1_101 : STD_LOGIC; 
  signal cnt_10_MC_D2_102 : STD_LOGIC; 
  signal N_PZ_178_105 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_0_112 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_1_115 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D_117 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q_118 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D1_119 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_120 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_0_121 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_1_122 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_2_123 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_3_124 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_4_125 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D_127 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_128 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D1_129 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_130 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_0_131 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_1_132 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_2_133 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_3_134 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_4_135 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D_137 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_138 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D1_139 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_140 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_0_141 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_1_142 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_2_143 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_3_144 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_4_145 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D_147 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_148 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D1_149 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_150 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_0_151 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_1_152 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_2_153 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_3_154 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_4_155 : STD_LOGIC; 
  signal N_PZ_178_MC_Q_156 : STD_LOGIC; 
  signal N_PZ_178_MC_D_157 : STD_LOGIC; 
  signal N_PZ_178_MC_D1_158 : STD_LOGIC; 
  signal N_PZ_178_MC_D2_159 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D_161 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_162 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D1_163 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_164 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_0_165 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_1_166 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_2_167 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_3_168 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_4_169 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D_171 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_172 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D1_173 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_174 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_0_175 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_1_176 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_2_177 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_3_178 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_4_179 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D_181 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q_182 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D1_183 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_184 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_0_185 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_1_186 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_2_187 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_3_188 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_4_189 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D_191 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_192 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D1_193 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_194 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_0_195 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_1_196 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_2_197 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_3_198 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_4_199 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D_201 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_202 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D1_203 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_204 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_0_205 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_1_206 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_2_207 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_3_208 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_4_209 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D_211 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q_212 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D1_213 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_214 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_0_215 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_1_216 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_2_217 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_3_218 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_4_219 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_D_221 : STD_LOGIC; 
  signal cnt_11_MC_D1_222 : STD_LOGIC; 
  signal cnt_11_MC_D2_223 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_225 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_226 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_227 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_228 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_229 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_4_230 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D_232 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q_233 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D1_234 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_235 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_0_236 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_1_237 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_2_238 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_3_239 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_4_240 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_Q_241 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_D_242 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_D1_243 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_D2_244 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_D2_PT_0_245 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_D2_PT_1_246 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_D2_PT_2_247 : STD_LOGIC; 
  signal cnt_1_err_cs_or0000_MC_D2_PT_3_248 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_250 : STD_LOGIC; 
  signal cnt_1_MC_D1_251 : STD_LOGIC; 
  signal cnt_1_MC_D2_252 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_253 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_254 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_2_255 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_257 : STD_LOGIC; 
  signal cnt_2_MC_D1_258 : STD_LOGIC; 
  signal cnt_2_MC_D2_259 : STD_LOGIC; 
  signal N_PZ_129_260 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_261 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_262 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_263 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_264 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_4_265 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_5_266 : STD_LOGIC; 
  signal N_PZ_129_MC_Q_267 : STD_LOGIC; 
  signal N_PZ_129_MC_D_268 : STD_LOGIC; 
  signal N_PZ_129_MC_D1_269 : STD_LOGIC; 
  signal N_PZ_129_MC_D2_270 : STD_LOGIC; 
  signal N_PZ_129_MC_D2_PT_0_271 : STD_LOGIC; 
  signal N_PZ_129_MC_D2_PT_1_272 : STD_LOGIC; 
  signal N_PZ_129_MC_D2_PT_2_273 : STD_LOGIC; 
  signal N_PZ_129_MC_D2_PT_3_274 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_3_MC_D_276 : STD_LOGIC; 
  signal cnt_3_MC_D1_277 : STD_LOGIC; 
  signal cnt_3_MC_D2_278 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_0_279 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_1_280 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_2_281 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_3_282 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_4_MC_D_284 : STD_LOGIC; 
  signal cnt_4_MC_D1_285 : STD_LOGIC; 
  signal cnt_4_MC_D2_286 : STD_LOGIC; 
  signal N_PZ_164_287 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_0_288 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_1_289 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_2_290 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_3_291 : STD_LOGIC; 
  signal N_PZ_164_MC_Q_292 : STD_LOGIC; 
  signal N_PZ_164_MC_D_293 : STD_LOGIC; 
  signal N_PZ_164_MC_D1_294 : STD_LOGIC; 
  signal N_PZ_164_MC_D2_295 : STD_LOGIC; 
  signal N_PZ_164_MC_D2_PT_0_296 : STD_LOGIC; 
  signal N_PZ_164_MC_D2_PT_1_297 : STD_LOGIC; 
  signal N_PZ_164_MC_D2_PT_2_298 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_5_MC_D_300 : STD_LOGIC; 
  signal cnt_5_MC_D1_301 : STD_LOGIC; 
  signal cnt_5_MC_D2_302 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_0_303 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_1_304 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_2_305 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_3_306 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_4_307 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_5_308 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_310 : STD_LOGIC; 
  signal cnt_6_MC_D1_311 : STD_LOGIC; 
  signal cnt_6_MC_D2_312 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_313 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_314 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_2_315 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_3_316 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_4_317 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_5_318 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_6_319 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_7_320 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_7_MC_D_322 : STD_LOGIC; 
  signal cnt_7_MC_D1_323 : STD_LOGIC; 
  signal cnt_7_MC_D2_324 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_0_325 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_1_326 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_8_MC_D_328 : STD_LOGIC; 
  signal cnt_8_MC_D1_329 : STD_LOGIC; 
  signal cnt_8_MC_D2_330 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_0_331 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_1_332 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_9_MC_D_334 : STD_LOGIC; 
  signal cnt_9_MC_D1_335 : STD_LOGIC; 
  signal cnt_9_MC_D2_336 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_0_337 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_1_338 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_2_339 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q_340 : STD_LOGIC; 
  signal did_0_MC_D_341 : STD_LOGIC; 
  signal did_0_MC_D1_342 : STD_LOGIC; 
  signal did_0_MC_D2_343 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q_344 : STD_LOGIC; 
  signal did_1_MC_D_345 : STD_LOGIC; 
  signal did_1_MC_D1_346 : STD_LOGIC; 
  signal did_1_MC_D2_347 : STD_LOGIC; 
  signal did_2_MC_Q_tsimrenamed_net_Q_348 : STD_LOGIC; 
  signal did_2_MC_D_349 : STD_LOGIC; 
  signal did_2_MC_D1_350 : STD_LOGIC; 
  signal did_2_MC_D2_351 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_D_353 : STD_LOGIC; 
  signal err_MC_CE_354 : STD_LOGIC; 
  signal err_MC_D1_355 : STD_LOGIC; 
  signal err_MC_D2_356 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_168_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_178_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_178_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_178_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_178_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_178_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_164_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_CE_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_CE_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_168_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_168_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_178_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_178_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_178_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_129_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_129_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_129_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_129_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_129_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_129_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_129_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_164_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal cnt_1_valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_5
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_7
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_9
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_11
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_13
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_15
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_17
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_19
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_21
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_23
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_25
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_27
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_29
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_35,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_37,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_39,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_41,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_43,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_45,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_47,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_49,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_51,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_53,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_55,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_57,
      O => cnt(9)
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_59,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_61,
      O => did(1)
    );
  did_2_Q : X_BUF
    port map (
      I => did_2_MC_Q_63,
      O => did(2)
    );
  err_66 : X_BUF
    port map (
      I => err_MC_Q_65,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_35
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_68
    );
  Vcc : X_ONE
    port map (
      O => Vcc_69
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_67
    );
  cnt_0_MC_D1 : X_ZERO
    port map (
      O => cnt_0_MC_D1_70
    );
  cnt_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2,
      O => cnt_0_MC_D2_PT_0_74
    );
  cnt_0_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2,
      O => cnt_0_MC_D2_PT_1_75
    );
  cnt_0_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      O => cnt_0_MC_D2_71
    );
  cnt_1_down_cs : X_BUF
    port map (
      I => cnt_1_down_cs_MC_Q,
      O => cnt_1_down_cs_72
    );
  cnt_1_down_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_down_cs_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_down_cs_MC_Q
    );
  cnt_1_down_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_down_cs_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_down_cs_MC_D_IN1,
      O => cnt_1_down_cs_MC_D_77
    );
  cnt_1_down_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1,
      O => cnt_1_down_cs_MC_D1_78
    );
  cnt_1_down_cs_MC_D2 : X_ZERO
    port map (
      O => cnt_1_down_cs_MC_D2_79
    );
  N_PZ_168 : X_BUF
    port map (
      I => N_PZ_168_MC_Q_80,
      O => N_PZ_168_73
    );
  N_PZ_168_MC_Q : X_BUF
    port map (
      I => N_PZ_168_MC_D_81,
      O => N_PZ_168_MC_Q_80
    );
  N_PZ_168_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_168_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_168_MC_D_IN1,
      O => N_PZ_168_MC_D_81
    );
  N_PZ_168_MC_D1 : X_ZERO
    port map (
      O => N_PZ_168_MC_D1_82
    );
  N_PZ_168_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_168_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_168_MC_D2_PT_0_IN1,
      O => N_PZ_168_MC_D2_PT_0_86
    );
  N_PZ_168_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_168_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_168_MC_D2_PT_1_IN1,
      O => N_PZ_168_MC_D2_PT_1_87
    );
  N_PZ_168_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_168_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_168_MC_D2_IN1,
      O => N_PZ_168_MC_D2_83
    );
  cnt_1_valLd_cs_0_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_Q,
      O => cnt_1_valLd_cs(0)
    );
  cnt_1_valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_0_MC_Q
    );
  cnt_1_valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1,
      O => cnt_1_valLd_cs_0_MC_D_89
    );
  cnt_1_valLd_cs_0_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_0_MC_D1_90
    );
  cnt_1_valLd_cs_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2,
      O => cnt_1_valLd_cs_0_MC_D2_PT_0_92
    );
  cnt_1_valLd_cs_0_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3,
      O => cnt_1_valLd_cs_0_MC_D2_PT_1_93
    );
  cnt_1_valLd_cs_0_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_0_MC_D2_PT_2_94
    );
  cnt_1_valLd_cs_0_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN2,
      O => cnt_1_valLd_cs_0_MC_D2_91
    );
  cnt_1_up_cs : X_BUF
    port map (
      I => cnt_1_up_cs_MC_Q,
      O => cnt_1_up_cs_85
    );
  cnt_1_up_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_up_cs_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_up_cs_MC_Q
    );
  cnt_1_up_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_up_cs_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_up_cs_MC_D_IN1,
      O => cnt_1_up_cs_MC_D_96
    );
  cnt_1_up_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1,
      O => cnt_1_up_cs_MC_D1_97
    );
  cnt_1_up_cs_MC_D2 : X_ZERO
    port map (
      O => cnt_1_up_cs_MC_D2_98
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_Q_37
    );
  cnt_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_10_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_10_MC_Q_tsimrenamed_net_Q
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_100
    );
  cnt_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D1_IN1,
      O => cnt_10_MC_D1_101
    );
  cnt_10_MC_D2_PT_0 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN6,
      I7 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN7,
      I8 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN8,
      I9 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN9,
      I10 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN10,
      I11 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN11,
      I12 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN12,
      I13 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN13,
      I14 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN14,
      I15 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN15,
      O => cnt_10_MC_D2_PT_0_112
    );
  cnt_10_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN7,
      I8 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN15,
      O => cnt_10_MC_D2_PT_1_115
    );
  cnt_10_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_IN1,
      O => cnt_10_MC_D2_102
    );
  cnt_1_valLd_cs_10_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_Q,
      O => cnt_1_valLd_cs(10)
    );
  cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q_118
    );
  cnt_1_valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_10_MC_Q
    );
  cnt_1_valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1,
      O => cnt_1_valLd_cs_10_MC_D_117
    );
  cnt_1_valLd_cs_10_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_10_MC_D1_119
    );
  cnt_1_valLd_cs_10_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_10_MC_D2_PT_0_121
    );
  cnt_1_valLd_cs_10_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_10_MC_D2_PT_1_122
    );
  cnt_1_valLd_cs_10_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_10_MC_D2_PT_2_123
    );
  cnt_1_valLd_cs_10_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10,
      I11 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_10_MC_D2_PT_3_124
    );
  cnt_1_valLd_cs_10_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_10_MC_D2_PT_4_125
    );
  cnt_1_valLd_cs_10_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN4,
      O => cnt_1_valLd_cs_10_MC_D2_120
    );
  cnt_1_valLd_cs_3_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_Q,
      O => cnt_1_valLd_cs(3)
    );
  cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_128
    );
  cnt_1_valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_3_MC_Q
    );
  cnt_1_valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1,
      O => cnt_1_valLd_cs_3_MC_D_127
    );
  cnt_1_valLd_cs_3_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_3_MC_D1_129
    );
  cnt_1_valLd_cs_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_3_MC_D2_PT_0_131
    );
  cnt_1_valLd_cs_3_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_3_MC_D2_PT_1_132
    );
  cnt_1_valLd_cs_3_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_3_MC_D2_PT_2_133
    );
  cnt_1_valLd_cs_3_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4,
      O => cnt_1_valLd_cs_3_MC_D2_PT_3_134
    );
  cnt_1_valLd_cs_3_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6,
      O => cnt_1_valLd_cs_3_MC_D2_PT_4_135
    );
  cnt_1_valLd_cs_3_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4,
      O => cnt_1_valLd_cs_3_MC_D2_130
    );
  cnt_1_valLd_cs_1_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_Q,
      O => cnt_1_valLd_cs(1)
    );
  cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_138
    );
  cnt_1_valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_1_MC_Q
    );
  cnt_1_valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1,
      O => cnt_1_valLd_cs_1_MC_D_137
    );
  cnt_1_valLd_cs_1_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_1_MC_D1_139
    );
  cnt_1_valLd_cs_1_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_1_MC_D2_PT_0_141
    );
  cnt_1_valLd_cs_1_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_1_MC_D2_PT_1_142
    );
  cnt_1_valLd_cs_1_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_1_MC_D2_PT_2_143
    );
  cnt_1_valLd_cs_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_PT_3_144
    );
  cnt_1_valLd_cs_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_PT_4_145
    );
  cnt_1_valLd_cs_1_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_140
    );
  cnt_1_valLd_cs_2_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_Q,
      O => cnt_1_valLd_cs(2)
    );
  cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_148
    );
  cnt_1_valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_2_MC_Q
    );
  cnt_1_valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1,
      O => cnt_1_valLd_cs_2_MC_D_147
    );
  cnt_1_valLd_cs_2_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_2_MC_D1_149
    );
  cnt_1_valLd_cs_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_2_MC_D2_PT_0_151
    );
  cnt_1_valLd_cs_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_2_MC_D2_PT_1_152
    );
  cnt_1_valLd_cs_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_2_MC_D2_PT_2_153
    );
  cnt_1_valLd_cs_2_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5,
      O => cnt_1_valLd_cs_2_MC_D2_PT_3_154
    );
  cnt_1_valLd_cs_2_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5,
      O => cnt_1_valLd_cs_2_MC_D2_PT_4_155
    );
  cnt_1_valLd_cs_2_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4,
      O => cnt_1_valLd_cs_2_MC_D2_150
    );
  N_PZ_178 : X_BUF
    port map (
      I => N_PZ_178_MC_Q_156,
      O => N_PZ_178_105
    );
  N_PZ_178_MC_Q : X_BUF
    port map (
      I => N_PZ_178_MC_D_157,
      O => N_PZ_178_MC_Q_156
    );
  N_PZ_178_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_178_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_178_MC_D_IN1,
      O => N_PZ_178_MC_D_157
    );
  N_PZ_178_MC_D1 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_178_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_178_MC_D1_IN1,
      I2 => NlwInverterSignal_N_PZ_178_MC_D1_IN2,
      O => N_PZ_178_MC_D1_158
    );
  N_PZ_178_MC_D2 : X_ZERO
    port map (
      O => N_PZ_178_MC_D2_159
    );
  cnt_1_valLd_cs_5_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_Q,
      O => cnt_1_valLd_cs(5)
    );
  cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_162
    );
  cnt_1_valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_5_MC_Q
    );
  cnt_1_valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1,
      O => cnt_1_valLd_cs_5_MC_D_161
    );
  cnt_1_valLd_cs_5_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_5_MC_D1_163
    );
  cnt_1_valLd_cs_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_5_MC_D2_PT_0_165
    );
  cnt_1_valLd_cs_5_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_5_MC_D2_PT_1_166
    );
  cnt_1_valLd_cs_5_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_5_MC_D2_PT_2_167
    );
  cnt_1_valLd_cs_5_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6,
      O => cnt_1_valLd_cs_5_MC_D2_PT_3_168
    );
  cnt_1_valLd_cs_5_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_5_MC_D2_PT_4_169
    );
  cnt_1_valLd_cs_5_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4,
      O => cnt_1_valLd_cs_5_MC_D2_164
    );
  cnt_1_valLd_cs_4_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_Q,
      O => cnt_1_valLd_cs(4)
    );
  cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_172
    );
  cnt_1_valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_4_MC_Q
    );
  cnt_1_valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1,
      O => cnt_1_valLd_cs_4_MC_D_171
    );
  cnt_1_valLd_cs_4_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_4_MC_D1_173
    );
  cnt_1_valLd_cs_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_4_MC_D2_PT_0_175
    );
  cnt_1_valLd_cs_4_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_4_MC_D2_PT_1_176
    );
  cnt_1_valLd_cs_4_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_4_MC_D2_PT_2_177
    );
  cnt_1_valLd_cs_4_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5,
      O => cnt_1_valLd_cs_4_MC_D2_PT_3_178
    );
  cnt_1_valLd_cs_4_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7,
      O => cnt_1_valLd_cs_4_MC_D2_PT_4_179
    );
  cnt_1_valLd_cs_4_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4,
      O => cnt_1_valLd_cs_4_MC_D2_174
    );
  cnt_1_valLd_cs_8_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_Q,
      O => cnt_1_valLd_cs(8)
    );
  cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q_182
    );
  cnt_1_valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_8_MC_Q
    );
  cnt_1_valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1,
      O => cnt_1_valLd_cs_8_MC_D_181
    );
  cnt_1_valLd_cs_8_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_8_MC_D1_183
    );
  cnt_1_valLd_cs_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_8_MC_D2_PT_0_185
    );
  cnt_1_valLd_cs_8_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_8_MC_D2_PT_1_186
    );
  cnt_1_valLd_cs_8_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_8_MC_D2_PT_2_187
    );
  cnt_1_valLd_cs_8_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_8_MC_D2_PT_3_188
    );
  cnt_1_valLd_cs_8_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_8_MC_D2_PT_4_189
    );
  cnt_1_valLd_cs_8_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4,
      O => cnt_1_valLd_cs_8_MC_D2_184
    );
  cnt_1_valLd_cs_6_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_Q,
      O => cnt_1_valLd_cs(6)
    );
  cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_192
    );
  cnt_1_valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_6_MC_Q
    );
  cnt_1_valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1,
      O => cnt_1_valLd_cs_6_MC_D_191
    );
  cnt_1_valLd_cs_6_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_6_MC_D1_193
    );
  cnt_1_valLd_cs_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_6_MC_D2_PT_0_195
    );
  cnt_1_valLd_cs_6_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_6_MC_D2_PT_1_196
    );
  cnt_1_valLd_cs_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_6_MC_D2_PT_2_197
    );
  cnt_1_valLd_cs_6_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7,
      O => cnt_1_valLd_cs_6_MC_D2_PT_3_198
    );
  cnt_1_valLd_cs_6_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_6_MC_D2_PT_4_199
    );
  cnt_1_valLd_cs_6_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4,
      O => cnt_1_valLd_cs_6_MC_D2_194
    );
  cnt_1_valLd_cs_7_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_Q,
      O => cnt_1_valLd_cs(7)
    );
  cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_202
    );
  cnt_1_valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_7_MC_Q
    );
  cnt_1_valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1,
      O => cnt_1_valLd_cs_7_MC_D_201
    );
  cnt_1_valLd_cs_7_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_7_MC_D1_203
    );
  cnt_1_valLd_cs_7_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_7_MC_D2_PT_0_205
    );
  cnt_1_valLd_cs_7_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_7_MC_D2_PT_1_206
    );
  cnt_1_valLd_cs_7_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_7_MC_D2_PT_2_207
    );
  cnt_1_valLd_cs_7_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_7_MC_D2_PT_3_208
    );
  cnt_1_valLd_cs_7_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_7_MC_D2_PT_4_209
    );
  cnt_1_valLd_cs_7_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4,
      O => cnt_1_valLd_cs_7_MC_D2_204
    );
  cnt_1_valLd_cs_9_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_Q,
      O => cnt_1_valLd_cs(9)
    );
  cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q_212
    );
  cnt_1_valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_9_MC_Q
    );
  cnt_1_valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1,
      O => cnt_1_valLd_cs_9_MC_D_211
    );
  cnt_1_valLd_cs_9_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_9_MC_D1_213
    );
  cnt_1_valLd_cs_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_9_MC_D2_PT_0_215
    );
  cnt_1_valLd_cs_9_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_9_MC_D2_PT_1_216
    );
  cnt_1_valLd_cs_9_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_9_MC_D2_PT_2_217
    );
  cnt_1_valLd_cs_9_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_9_MC_D2_PT_3_218
    );
  cnt_1_valLd_cs_9_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_9_MC_D2_PT_4_219
    );
  cnt_1_valLd_cs_9_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN4,
      O => cnt_1_valLd_cs_9_MC_D2_214
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_39
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_221
    );
  cnt_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D1_IN1,
      O => cnt_11_MC_D1_222
    );
  cnt_11_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN3,
      O => cnt_11_MC_D2_PT_0_226
    );
  cnt_11_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3,
      O => cnt_11_MC_D2_PT_1_227
    );
  cnt_11_MC_D2_PT_2 : X_AND7
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN6,
      O => cnt_11_MC_D2_PT_2_228
    );
  cnt_11_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN11,
      I12 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN15,
      O => cnt_11_MC_D2_PT_3_229
    );
  cnt_11_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN12,
      I13 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN13,
      I14 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN15,
      O => cnt_11_MC_D2_PT_4_230
    );
  cnt_11_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_IN4,
      O => cnt_11_MC_D2_223
    );
  cnt_1_valLd_cs_11_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_Q,
      O => cnt_1_valLd_cs(11)
    );
  cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q_233
    );
  cnt_1_valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_11_MC_Q
    );
  cnt_1_valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1,
      O => cnt_1_valLd_cs_11_MC_D_232
    );
  cnt_1_valLd_cs_11_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_11_MC_D1_234
    );
  cnt_1_valLd_cs_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_11_MC_D2_PT_0_236
    );
  cnt_1_valLd_cs_11_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_11_MC_D2_PT_1_237
    );
  cnt_1_valLd_cs_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_11_MC_D2_PT_2_238
    );
  cnt_1_valLd_cs_11_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10,
      I11 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11,
      I12 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_11_MC_D2_PT_3_239
    );
  cnt_1_valLd_cs_11_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_11_MC_D2_PT_4_240
    );
  cnt_1_valLd_cs_11_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN4,
      O => cnt_1_valLd_cs_11_MC_D2_235
    );
  cnt_1_err_cs_or0000 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_Q_241,
      O => cnt_1_err_cs_or0000_225
    );
  cnt_1_err_cs_or0000_MC_Q : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_D_242,
      O => cnt_1_err_cs_or0000_MC_Q_241
    );
  cnt_1_err_cs_or0000_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D_IN1,
      O => cnt_1_err_cs_or0000_MC_D_242
    );
  cnt_1_err_cs_or0000_MC_D1 : X_ZERO
    port map (
      O => cnt_1_err_cs_or0000_MC_D1_243
    );
  cnt_1_err_cs_or0000_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1,
      O => cnt_1_err_cs_or0000_MC_D2_PT_0_245
    );
  cnt_1_err_cs_or0000_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_1_IN1,
      O => cnt_1_err_cs_or0000_MC_D2_PT_1_246
    );
  cnt_1_err_cs_or0000_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8,
      I9 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9,
      I10 => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN15,
      O => cnt_1_err_cs_or0000_MC_D2_PT_2_247
    );
  cnt_1_err_cs_or0000_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN15,
      O => cnt_1_err_cs_or0000_MC_D2_PT_3_248
    );
  cnt_1_err_cs_or0000_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN3,
      O => cnt_1_err_cs_or0000_MC_D2_244
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_41
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_250
    );
  cnt_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D1_IN1,
      O => cnt_1_MC_D1_251
    );
  cnt_1_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3,
      O => cnt_1_MC_D2_PT_0_253
    );
  cnt_1_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN4,
      O => cnt_1_MC_D2_PT_1_254
    );
  cnt_1_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN5,
      O => cnt_1_MC_D2_PT_2_255
    );
  cnt_1_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_IN2,
      O => cnt_1_MC_D2_252
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_43
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_257
    );
  cnt_2_MC_D1 : X_ZERO
    port map (
      O => cnt_2_MC_D1_258
    );
  cnt_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      O => cnt_2_MC_D2_PT_0_261
    );
  cnt_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2,
      O => cnt_2_MC_D2_PT_1_262
    );
  cnt_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3,
      O => cnt_2_MC_D2_PT_2_263
    );
  cnt_2_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      O => cnt_2_MC_D2_PT_3_264
    );
  cnt_2_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3,
      O => cnt_2_MC_D2_PT_4_265
    );
  cnt_2_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4,
      O => cnt_2_MC_D2_PT_5_266
    );
  cnt_2_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_IN5,
      O => cnt_2_MC_D2_259
    );
  N_PZ_129 : X_BUF
    port map (
      I => N_PZ_129_MC_Q_267,
      O => N_PZ_129_260
    );
  N_PZ_129_MC_Q : X_BUF
    port map (
      I => N_PZ_129_MC_D_268,
      O => N_PZ_129_MC_Q_267
    );
  N_PZ_129_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_129_MC_D_IN1,
      O => N_PZ_129_MC_D_268
    );
  N_PZ_129_MC_D1 : X_ZERO
    port map (
      O => N_PZ_129_MC_D1_269
    );
  N_PZ_129_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_129_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_N_PZ_129_MC_D2_PT_0_IN2,
      O => N_PZ_129_MC_D2_PT_0_271
    );
  N_PZ_129_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_129_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_129_MC_D2_PT_1_IN2,
      O => N_PZ_129_MC_D2_PT_1_272
    );
  N_PZ_129_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_N_PZ_129_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_129_MC_D2_PT_2_IN2,
      O => N_PZ_129_MC_D2_PT_2_273
    );
  N_PZ_129_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_N_PZ_129_MC_D2_PT_3_IN2,
      O => N_PZ_129_MC_D2_PT_3_274
    );
  N_PZ_129_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_129_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_129_MC_D2_IN2,
      I3 => NlwBufferSignal_N_PZ_129_MC_D2_IN3,
      O => N_PZ_129_MC_D2_270
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_Q_45
    );
  cnt_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_3_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_3_MC_Q_tsimrenamed_net_Q
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_276
    );
  cnt_3_MC_D1 : X_ZERO
    port map (
      O => cnt_3_MC_D1_277
    );
  cnt_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1,
      O => cnt_3_MC_D2_PT_0_279
    );
  cnt_3_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN3,
      O => cnt_3_MC_D2_PT_1_280
    );
  cnt_3_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4,
      O => cnt_3_MC_D2_PT_2_281
    );
  cnt_3_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6,
      O => cnt_3_MC_D2_PT_3_282
    );
  cnt_3_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_IN3,
      O => cnt_3_MC_D2_278
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_Q_47
    );
  cnt_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_4_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_4_MC_Q_tsimrenamed_net_Q
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_284
    );
  cnt_4_MC_D1 : X_ZERO
    port map (
      O => cnt_4_MC_D1_285
    );
  cnt_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1,
      O => cnt_4_MC_D2_PT_0_288
    );
  cnt_4_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3,
      O => cnt_4_MC_D2_PT_1_289
    );
  cnt_4_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN5,
      O => cnt_4_MC_D2_PT_2_290
    );
  cnt_4_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7,
      O => cnt_4_MC_D2_PT_3_291
    );
  cnt_4_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_IN3,
      O => cnt_4_MC_D2_286
    );
  N_PZ_164 : X_BUF
    port map (
      I => N_PZ_164_MC_Q_292,
      O => N_PZ_164_287
    );
  N_PZ_164_MC_Q : X_BUF
    port map (
      I => N_PZ_164_MC_D_293,
      O => N_PZ_164_MC_Q_292
    );
  N_PZ_164_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_164_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_164_MC_D_IN1,
      O => N_PZ_164_MC_D_293
    );
  N_PZ_164_MC_D1 : X_ZERO
    port map (
      O => N_PZ_164_MC_D1_294
    );
  N_PZ_164_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_164_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_164_MC_D2_PT_0_IN1,
      O => N_PZ_164_MC_D2_PT_0_296
    );
  N_PZ_164_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN2,
      O => N_PZ_164_MC_D2_PT_1_297
    );
  N_PZ_164_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_164_MC_D2_PT_2_IN2,
      O => N_PZ_164_MC_D2_PT_2_298
    );
  N_PZ_164_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_N_PZ_164_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_164_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_164_MC_D2_IN2,
      O => N_PZ_164_MC_D2_295
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_Q_49
    );
  cnt_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_5_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_5_MC_Q_tsimrenamed_net_Q
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_300
    );
  cnt_5_MC_D1 : X_ZERO
    port map (
      O => cnt_5_MC_D1_301
    );
  cnt_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1,
      O => cnt_5_MC_D2_PT_0_303
    );
  cnt_5_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3,
      O => cnt_5_MC_D2_PT_1_304
    );
  cnt_5_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3,
      O => cnt_5_MC_D2_PT_2_305
    );
  cnt_5_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3,
      O => cnt_5_MC_D2_PT_3_306
    );
  cnt_5_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5,
      O => cnt_5_MC_D2_PT_4_307
    );
  cnt_5_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN5,
      O => cnt_5_MC_D2_PT_5_308
    );
  cnt_5_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_IN5,
      O => cnt_5_MC_D2_302
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_51
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_310
    );
  cnt_6_MC_D1 : X_ZERO
    port map (
      O => cnt_6_MC_D1_311
    );
  cnt_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      O => cnt_6_MC_D2_PT_0_313
    );
  cnt_6_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3,
      O => cnt_6_MC_D2_PT_1_314
    );
  cnt_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3,
      O => cnt_6_MC_D2_PT_2_315
    );
  cnt_6_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3,
      O => cnt_6_MC_D2_PT_3_316
    );
  cnt_6_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3,
      O => cnt_6_MC_D2_PT_4_317
    );
  cnt_6_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN3,
      O => cnt_6_MC_D2_PT_5_318
    );
  cnt_6_MC_D2_PT_6 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6,
      O => cnt_6_MC_D2_PT_6_319
    );
  cnt_6_MC_D2_PT_7 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN6,
      O => cnt_6_MC_D2_PT_7_320
    );
  cnt_6_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_IN7,
      O => cnt_6_MC_D2_312
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_Q_53
    );
  cnt_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_7_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_7_MC_Q_tsimrenamed_net_Q
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_322
    );
  cnt_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D1_IN1,
      O => cnt_7_MC_D1_323
    );
  cnt_7_MC_D2_PT_0 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN6,
      I7 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN7,
      O => cnt_7_MC_D2_PT_0_325
    );
  cnt_7_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN7,
      I8 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN15,
      O => cnt_7_MC_D2_PT_1_326
    );
  cnt_7_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_IN1,
      O => cnt_7_MC_D2_324
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_Q_55
    );
  cnt_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_8_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_8_MC_Q_tsimrenamed_net_Q
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_328
    );
  cnt_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D1_IN1,
      O => cnt_8_MC_D1_329
    );
  cnt_8_MC_D2_PT_0 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN6,
      I7 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN7,
      I8 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN8,
      I9 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN9,
      I10 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN10,
      I11 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN15,
      O => cnt_8_MC_D2_PT_0_331
    );
  cnt_8_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN7,
      I8 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN15,
      O => cnt_8_MC_D2_PT_1_332
    );
  cnt_8_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_IN1,
      O => cnt_8_MC_D2_330
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_Q_57
    );
  cnt_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_9_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_9_MC_Q_tsimrenamed_net_Q
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_334
    );
  cnt_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D1_IN1,
      O => cnt_9_MC_D1_335
    );
  cnt_9_MC_D2_PT_0 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN6,
      I7 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN7,
      I8 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN8,
      I9 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN9,
      I10 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN10,
      I11 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN11,
      I12 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN15,
      O => cnt_9_MC_D2_PT_0_337
    );
  cnt_9_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN7,
      I8 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN8,
      I9 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN15,
      O => cnt_9_MC_D2_PT_1_338
    );
  cnt_9_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN7,
      I8 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN15,
      O => cnt_9_MC_D2_PT_2_339
    );
  cnt_9_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_IN2,
      O => cnt_9_MC_D2_336
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q_340,
      O => did_0_MC_Q_59
    );
  did_0_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_0_MC_D_341,
      O => did_0_MC_Q_tsimrenamed_net_Q_340
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_341
    );
  did_0_MC_D1 : X_ZERO
    port map (
      O => did_0_MC_D1_342
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_343
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q_344,
      O => did_1_MC_Q_61
    );
  did_1_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_1_MC_D_345,
      O => did_1_MC_Q_tsimrenamed_net_Q_344
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_345
    );
  did_1_MC_D1 : X_ZERO
    port map (
      O => did_1_MC_D1_346
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_347
    );
  did_2_MC_Q : X_BUF
    port map (
      I => did_2_MC_Q_tsimrenamed_net_Q_348,
      O => did_2_MC_Q_63
    );
  did_2_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_2_MC_D_349,
      O => did_2_MC_Q_tsimrenamed_net_Q_348
    );
  did_2_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_did_2_MC_D_IN0,
      I1 => NlwBufferSignal_did_2_MC_D_IN1,
      O => did_2_MC_D_349
    );
  did_2_MC_D1 : X_ZERO
    port map (
      O => did_2_MC_D1_350
    );
  did_2_MC_D2 : X_ZERO
    port map (
      O => did_2_MC_D2_351
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_65
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => err_MC_CE_354,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_353
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_355
    );
  err_MC_D2 : X_AND2
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      O => err_MC_D2_356
    );
  err_MC_CE : X_AND2
    port map (
      I0 => NlwBufferSignal_err_MC_CE_IN0,
      I1 => NlwBufferSignal_err_MC_CE_IN1,
      O => err_MC_CE_354
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_67,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_70,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_71,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_168_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_168_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_74,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_75,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_down_cs_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D_77,
      O => NlwBufferSignal_cnt_1_down_cs_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_down_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D1_78,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D2_79,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_5,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_168_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_168_MC_D1_82,
      O => NlwBufferSignal_N_PZ_168_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_168_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_168_MC_D2_83,
      O => NlwBufferSignal_N_PZ_168_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_168_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_168_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_168_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_N_PZ_168_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_168_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_168_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_168_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_N_PZ_168_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_168_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_168_MC_D2_PT_0_86,
      O => NlwBufferSignal_N_PZ_168_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_168_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_168_MC_D2_PT_1_87,
      O => NlwBufferSignal_N_PZ_168_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D_89,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D1_90,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_91,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_0_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_168_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_168_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_0_92,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_1_93,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_2_94,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_up_cs_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D_96,
      O => NlwBufferSignal_cnt_1_up_cs_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_up_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D1_97,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D2_98,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_11,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_MC_D_100,
      O => NlwBufferSignal_cnt_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_101,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_102,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN8
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN9
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN10
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN11
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN12
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN13
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN14
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN15
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_0_112,
      O => NlwBufferSignal_cnt_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_1_115,
      O => NlwBufferSignal_cnt_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D_117,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q_118,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D1_119,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_120,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_10_II_UIM_13,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_10_II_UIM_13,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_0_121,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_1_122,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_2_123,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_3_124,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_4_125,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D_127,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_128,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D1_129,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_130,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_3_II_UIM_15,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_3_II_UIM_15,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_0_131,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_1_132,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_2_133,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_3_134,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_4_135,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D_137,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_138,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D1_139,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_140,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_1_II_UIM_17,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_1_II_UIM_17,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_0_141,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_1_142,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_2_143,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_3_144,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_4_145,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D_147,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_148,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D1_149,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_150,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_2_II_UIM_19,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_2_II_UIM_19,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_0_151,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_1_152,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_2_153,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_3_154,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_4_155,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4
    );
  NlwBufferBlock_N_PZ_178_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_178_MC_D1_158,
      O => NlwBufferSignal_N_PZ_178_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_178_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_178_MC_D2_159,
      O => NlwBufferSignal_N_PZ_178_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_178_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_178_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_178_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_178_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_178_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_178_MC_D1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D_161,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_162,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D1_163,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_164,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_5_II_UIM_21,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_5_II_UIM_21,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_0_165,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_1_166,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_2_167,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_3_168,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_4_169,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D_171,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_172,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D1_173,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_174,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_4_II_UIM_23,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_4_II_UIM_23,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_0_175,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_1_176,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_2_177,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_3_178,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_4_179,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D_181,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q_182,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D1_183,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_184,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_8_II_UIM_25,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_8_II_UIM_25,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_0_185,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_1_186,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_2_187,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_3_188,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_4_189,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D_191,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_192,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D1_193,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_194,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_6_II_UIM_27,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_6_II_UIM_27,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_0_195,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_1_196,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_2_197,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_3_198,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_4_199,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D_201,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_202,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D1_203,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_204,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_7_II_UIM_29,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_7_II_UIM_29,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_0_205,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_1_206,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_2_207,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_3_208,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_4_209,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D_211,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q_212,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D1_213,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_214,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_9_II_UIM_31,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_9_II_UIM_31,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_0_215,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_1_216,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_2_217,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_3_218,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_4_219,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN4
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_D_221,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_222,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_223,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_cnt_11_MC_D1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_226,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_227,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_228,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_229,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_4_230,
      O => NlwBufferSignal_cnt_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D_232,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q_233,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D1_234,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_235,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_7,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_0_236,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_1_237,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_2_238,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_3_239,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_4_240,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_D1_243,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_D2_244,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_D2_PT_0_245,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_D2_PT_1_246,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_D2_PT_2_247,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_err_cs_or0000_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_MC_D2_PT_3_248,
      O => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_D_250,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_251,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_252,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_168_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_168_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_168_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_253,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_254,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_2_255,
      O => NlwBufferSignal_cnt_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_D_257,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_258,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_259,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_261,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_262,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_263,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_264,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_4_265,
      O => NlwBufferSignal_cnt_2_MC_D2_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_5_266,
      O => NlwBufferSignal_cnt_2_MC_D2_IN5
    );
  NlwBufferBlock_N_PZ_129_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_129_MC_D1_269,
      O => NlwBufferSignal_N_PZ_129_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_129_MC_D2_270,
      O => NlwBufferSignal_N_PZ_129_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_N_PZ_129_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_129_MC_D2_PT_0_271,
      O => NlwBufferSignal_N_PZ_129_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_129_MC_D2_PT_1_272,
      O => NlwBufferSignal_N_PZ_129_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_129_MC_D2_PT_2_273,
      O => NlwBufferSignal_N_PZ_129_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_129_MC_D2_IN3 : X_BUF
    port map (
      I => N_PZ_129_MC_D2_PT_3_274,
      O => NlwBufferSignal_N_PZ_129_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_MC_D_276,
      O => NlwBufferSignal_cnt_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_277,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_278,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_0_279,
      O => NlwBufferSignal_cnt_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_1_280,
      O => NlwBufferSignal_cnt_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_2_281,
      O => NlwBufferSignal_cnt_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_3_282,
      O => NlwBufferSignal_cnt_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_MC_D_284,
      O => NlwBufferSignal_cnt_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_285,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_286,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_164_287,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_0_288,
      O => NlwBufferSignal_cnt_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_1_289,
      O => NlwBufferSignal_cnt_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_2_290,
      O => NlwBufferSignal_cnt_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_3_291,
      O => NlwBufferSignal_cnt_4_MC_D2_IN3
    );
  NlwBufferBlock_N_PZ_164_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_164_MC_D1_294,
      O => NlwBufferSignal_N_PZ_164_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_164_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_164_MC_D2_295,
      O => NlwBufferSignal_N_PZ_164_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_164_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_164_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_164_MC_D2_PT_0_296,
      O => NlwBufferSignal_N_PZ_164_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_164_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_164_MC_D2_PT_1_297,
      O => NlwBufferSignal_N_PZ_164_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_164_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_164_MC_D2_PT_2_298,
      O => NlwBufferSignal_N_PZ_164_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_MC_D_300,
      O => NlwBufferSignal_cnt_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_301,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_302,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_164_287,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_0_303,
      O => NlwBufferSignal_cnt_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_1_304,
      O => NlwBufferSignal_cnt_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_2_305,
      O => NlwBufferSignal_cnt_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_3_306,
      O => NlwBufferSignal_cnt_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_4_307,
      O => NlwBufferSignal_cnt_5_MC_D2_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_5_308,
      O => NlwBufferSignal_cnt_5_MC_D2_IN5
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_D_310,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_311,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_312,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_164_287,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_6_IN6 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => N_PZ_129_260,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_313,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_314,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_2_315,
      O => NlwBufferSignal_cnt_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_3_316,
      O => NlwBufferSignal_cnt_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_4_317,
      O => NlwBufferSignal_cnt_6_MC_D2_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_5_318,
      O => NlwBufferSignal_cnt_6_MC_D2_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_6_319,
      O => NlwBufferSignal_cnt_6_MC_D2_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_7_320,
      O => NlwBufferSignal_cnt_6_MC_D2_IN7
    );
  NlwBufferBlock_cnt_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_MC_D_322,
      O => NlwBufferSignal_cnt_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_323,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_324,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_0_325,
      O => NlwBufferSignal_cnt_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_1_326,
      O => NlwBufferSignal_cnt_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_MC_D_328,
      O => NlwBufferSignal_cnt_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_329,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_330,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_0_331,
      O => NlwBufferSignal_cnt_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_1_332,
      O => NlwBufferSignal_cnt_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_MC_D_334,
      O => NlwBufferSignal_cnt_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_335,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_336,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => N_PZ_178_105,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_up_cs_85,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_0_337,
      O => NlwBufferSignal_cnt_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_1_338,
      O => NlwBufferSignal_cnt_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_2_339,
      O => NlwBufferSignal_cnt_9_MC_D2_IN2
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_342,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_343,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_346,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_347,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_did_2_MC_D_IN0 : X_BUF
    port map (
      I => did_2_MC_D1_350,
      O => NlwBufferSignal_did_2_MC_D_IN0
    );
  NlwBufferBlock_did_2_MC_D_IN1 : X_BUF
    port map (
      I => did_2_MC_D2_351,
      O => NlwBufferSignal_did_2_MC_D_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_353,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_355,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_356,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_CE_IN0 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_err_MC_CE_IN0
    );
  NlwBufferBlock_err_MC_CE_IN1 : X_BUF
    port map (
      I => cnt_1_err_cs_or0000_225,
      O => NlwBufferSignal_err_MC_CE_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_168_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_168_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_168_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_N_PZ_168_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_168_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_168_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN7,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN7
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN8,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN8
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN9,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN9
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN10,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN10
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_N_PZ_178_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_178_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_178_MC_D1_IN0
    );
  NlwInverterBlock_N_PZ_178_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_178_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_178_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_178_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_178_MC_D1_IN2,
      O => NlwInverterSignal_N_PZ_178_MC_D1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_11_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D1_IN1,
      O => NlwInverterSignal_cnt_11_MC_D1_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN12,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN12
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN13 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN13,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN13
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN14 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN14,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN14
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN9
    );
  NlwInverterBlock_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10,
      O => NlwInverterSignal_cnt_1_err_cs_or0000_MC_D2_PT_2_IN10
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_5_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN0
    );
  NlwInverterBlock_N_PZ_129_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_129_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_129_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_129_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_N_PZ_129_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_N_PZ_129_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_129_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_129_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_129_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_129_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_129_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_N_PZ_129_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_N_PZ_129_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_129_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_129_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_N_PZ_129_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_129_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_N_PZ_129_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_N_PZ_164_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_164_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_164_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_6_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_6_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_6_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_7_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_7_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_7_IN6
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN7,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN7
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN7,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN7
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN8,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN8
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN7,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN7
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN8,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN8
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN9,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN9
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN8,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN8
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN9,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN9
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_did_2_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_did_2_MC_D_IN0,
      O => NlwInverterSignal_did_2_MC_D_IN0
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure_atop;

