library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;


architecture aril of counter is
signal cnt_ns : std_logic_vector(11 downto 0);
signal valLd_ns : std_logic_vector(11 downto 0);
signal err_ns : std_logic;
signal up_ns : std_logic;
signal down_ns : std_logic;

signal cnt_cs : std_logic_vector(11 downto 0):= (others=>'0');
signal valLd_cs : std_logic_vector(11 downto 0):= (others=>'0');
signal err_cs : std_logic := '0';
signal up_cs : std_logic := '0';
signal down_cs : std_logic := '0';

begin

up_ns <= up;
down_ns <= down;
valLd_ns <= valLd;

reg:
    process (clk) is
    begin
        if clk = '1' and clk'event then
            if nres = '0'   then
                cnt_cs <= (others=>'0');
                valLd_cs <= (others=>'0');
                err_cs <= '0';
                up_cs <= '0';
                down_cs <= '0';
            else
                cnt_cs <= cnt_ns;
                if nLd = '0' then
                	valLd_cs <= valLd_ns;
                else
                	valLd_cs <= cnt_ns;
                end if;
                err_cs <= err_ns;
                up_cs <= up_ns;
                down_cs <= down_ns;
            end if;
        end if;
    end process reg;



counter:
	process(valLd_cs, up_cs, down_cs , err_cs) is
	variable y_v : std_logic;
    variable carry_v : std_logic;
    variable cnt_v : std_logic_vector(11 downto 0);
    variable err_v : std_logic;
	begin

    cnt_v := valLd_cs;
    err_v := err_cs;
    y_v := '0';
    carry_v := '0';

    if up_cs = '1' and down_cs = '0' then

        y_v := '0';
        cnt_v(0) := (valLd_cs(0) XOR up_cs) XOR '0';       -- (x ^ y) ^ ci
        carry_v := (valLd_cs(0) AND '0') Or (up_cs AND '0') OR (valLd_cs(0) AND up_cs); -- (x and ci) or (y and ci) or (x and y)

    elsif up_cs = '0' and down_cs = '1' then

        y_v := '1';
        cnt_v(0) := (valLd_cs(0) XOR (not down_cs)) XOR '1';
        carry_v := (valLd_cs(0) AND '1') OR ((not down_cs) AND '1') OR (valLd_cs(0) AND (not down_cs));

    end if;

    if up_cs /= down_cs then
        for I in 1 to 11 loop
        cnt_v(I) := (valLd_cs(I) XOR y_v) XOR carry_v;
        carry_v := (valLd_cs(I) AND carry_v) OR (y_v AND carry_v) OR (valLd_cs(I) AND y_v);
        end loop;
    end if;

    if up_cs = '1' and down_cs = '0' then
        err_v := err_v OR carry_v;
    elsif up_cs = '0' and down_cs = '1' then
        err_v := err_v OR (not carry_v);
    elsif up_cs = '1' and down_cs = '1' then
        err_v := '1';
    end if;

    err_ns <= err_v;
    cnt_ns <= cnt_v;

	end process counter;

cnt <= cnt_cs;
err <= err_cs;
did <= "001";


end architecture aril;