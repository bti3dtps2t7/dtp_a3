
library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;


entity DUT is                                 --Device Under test
  port ( 
    did   : out std_logic_vector(  2 downto 0 );  --Dut IDentifier
    err   : out std_logic;                        --ERRor : invalid counter value
    cnt   : out std_logic_vector( 11 downto 0 );  --CouNT
    -- 
    valLd : in  std_logic_vector( 11 downto 0 );  --init VALue in case of LoaD
    nLd   : in  std_logic;                        --Not LoaD; low actve LoaD
    up    : in  std_logic;                        --UP count command
    down  : in  std_logic;                        --DOWN count command
    -- 
    clk   : in  std_logic;                        --CLocK
    nres  : in  std_logic                         --Not RESet ; low active synchronous reset
  );--]port
end entity DUT;


architecture beh of DUT is

component counter
	port(
		did   : out std_logic_vector(  2 downto 0 );
    	err   : out std_logic;                        
    	cnt   : out std_logic_vector( 11 downto 0 );  
      	valLd : in  std_logic_vector( 11 downto 0 );  
    	nLd   : in  std_logic;                        
    	up    : in  std_logic;                        
    	down  : in  std_logic;                        
    	clk   : in  std_logic;                        
    	nres  : in  std_logic
		)
	;
end component;
for all : counter use entity work.counter ( aril );

begin

cnt_1 : counter
    port map (
        valLd => ValLd,
        nLd => nLd,
        up => up,
        down => down,
        clk => clk,
        nres => nres,
        did => did,
        err => err,
        cnt => cnt
    )--]port
;--]ha_i1




end architecture beh;

