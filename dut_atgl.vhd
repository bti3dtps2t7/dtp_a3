--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: DUT_timesim.vhd
-- /___/   /\     Timestamp: Fri Dec 11 14:48:56 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm DUT -w -dir netgen/fit -ofmt vhdl -sim DUT.nga DUT_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: DUT.nga
-- Output file	: D:\DTP\dtp_a3\dt_aufgabe3\ise_14x4\iseWRK\netgen\fit\DUT_timesim.vhd
-- # of Entities	: 1
-- Design Name	: DUT.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;


architecture Structure_atgl of DUT is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal down_II_UIM_5 : STD_LOGIC; 
  signal up_II_UIM_7 : STD_LOGIC; 
  signal nLd_II_UIM_9 : STD_LOGIC; 
  signal valLd_0_II_UIM_11 : STD_LOGIC; 
  signal valLd_10_II_UIM_13 : STD_LOGIC; 
  signal valLd_1_II_UIM_15 : STD_LOGIC; 
  signal valLd_8_II_UIM_17 : STD_LOGIC; 
  signal valLd_2_II_UIM_19 : STD_LOGIC; 
  signal valLd_3_II_UIM_21 : STD_LOGIC; 
  signal valLd_4_II_UIM_23 : STD_LOGIC; 
  signal valLd_5_II_UIM_25 : STD_LOGIC; 
  signal valLd_6_II_UIM_27 : STD_LOGIC; 
  signal valLd_7_II_UIM_29 : STD_LOGIC; 
  signal valLd_9_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal cnt_0_MC_Q_35 : STD_LOGIC; 
  signal cnt_10_MC_Q_37 : STD_LOGIC; 
  signal cnt_11_MC_Q_39 : STD_LOGIC; 
  signal cnt_1_MC_Q_41 : STD_LOGIC; 
  signal cnt_2_MC_Q_43 : STD_LOGIC; 
  signal cnt_3_MC_Q_45 : STD_LOGIC; 
  signal cnt_4_MC_Q_47 : STD_LOGIC; 
  signal cnt_5_MC_Q_49 : STD_LOGIC; 
  signal cnt_6_MC_Q_51 : STD_LOGIC; 
  signal cnt_7_MC_Q_53 : STD_LOGIC; 
  signal cnt_8_MC_Q_55 : STD_LOGIC; 
  signal cnt_9_MC_Q_57 : STD_LOGIC; 
  signal did_0_MC_Q_59 : STD_LOGIC; 
  signal did_1_MC_Q_61 : STD_LOGIC; 
  signal did_2_MC_Q_63 : STD_LOGIC; 
  signal err_MC_Q_65 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_D_67 : STD_LOGIC; 
  signal Gnd_68 : STD_LOGIC; 
  signal Vcc_69 : STD_LOGIC; 
  signal cnt_0_MC_D1_70 : STD_LOGIC; 
  signal cnt_0_MC_D2_71 : STD_LOGIC; 
  signal N_PZ_153_72 : STD_LOGIC; 
  signal N_PZ_153_MC_Q_73 : STD_LOGIC; 
  signal N_PZ_153_MC_D_74 : STD_LOGIC; 
  signal N_PZ_153_MC_D1_75 : STD_LOGIC; 
  signal N_PZ_153_MC_D2_76 : STD_LOGIC; 
  signal cnt_1_down_cs_77 : STD_LOGIC; 
  signal cnt_1_up_cs_78 : STD_LOGIC; 
  signal N_PZ_153_MC_D2_PT_0_80 : STD_LOGIC; 
  signal N_PZ_153_MC_D2_PT_1_81 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_Q : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D_83 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D1_84 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D2_85 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_Q : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D_87 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D1_88 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D2_89 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D_91 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D1_92 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_93 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_0_94 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_1_95 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_10_MC_D_97 : STD_LOGIC; 
  signal cnt_10_MC_D1_98 : STD_LOGIC; 
  signal cnt_10_MC_D2_99 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_0_101 : STD_LOGIC; 
  signal N_PZ_188_102 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_1_103 : STD_LOGIC; 
  signal N_PZ_193_104 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_2_105 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_3_107 : STD_LOGIC; 
  signal N_PZ_127_109 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_4_111 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_5_112 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D_114 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D1_115 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_116 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_0_117 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_1_118 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_2_119 : STD_LOGIC; 
  signal N_PZ_188_MC_Q_120 : STD_LOGIC; 
  signal N_PZ_188_MC_D_121 : STD_LOGIC; 
  signal N_PZ_188_MC_D1_122 : STD_LOGIC; 
  signal N_PZ_188_MC_D2_123 : STD_LOGIC; 
  signal N_PZ_129_124 : STD_LOGIC; 
  signal N_PZ_188_MC_D2_PT_0_125 : STD_LOGIC; 
  signal N_PZ_188_MC_D2_PT_1_126 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D_128 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_129 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D1_130 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_131 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_0_132 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_1_133 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_2_134 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_3_135 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_4_136 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D_138 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D1_139 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_140 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_0_141 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_1_142 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_2_143 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_3_144 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_4_145 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_5_146 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_6_147 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_7_148 : STD_LOGIC; 
  signal N_PZ_127_MC_Q_149 : STD_LOGIC; 
  signal N_PZ_127_MC_D_150 : STD_LOGIC; 
  signal N_PZ_127_MC_D1_151 : STD_LOGIC; 
  signal N_PZ_127_MC_D2_152 : STD_LOGIC; 
  signal N_PZ_116_154 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D_157 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_158 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D1_159 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_160 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_0_161 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_1_162 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_2_163 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_3_164 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_4_169 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D_171 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_172 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D1_173 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_174 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_0_175 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_1_176 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_2_177 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_3_178 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_4_179 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D_181 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_182 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D1_183 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_184 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_0_185 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_1_186 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_2_187 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_3_188 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_4_189 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D_191 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_192 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D1_193 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_194 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_0_195 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_1_196 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_2_197 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_3_198 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_4_199 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D_201 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_202 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D1_203 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_204 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_0_205 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_1_206 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_2_207 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_3_208 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_4_209 : STD_LOGIC; 
  signal N_PZ_116_MC_Q_210 : STD_LOGIC; 
  signal N_PZ_116_MC_D_211 : STD_LOGIC; 
  signal N_PZ_116_MC_D1_212 : STD_LOGIC; 
  signal N_PZ_116_MC_D2_213 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D_215 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_216 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D1_217 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_218 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_0_219 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_1_220 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_2_221 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_3_222 : STD_LOGIC; 
  signal N_PZ_122_223 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_4_224 : STD_LOGIC; 
  signal N_PZ_122_MC_Q_225 : STD_LOGIC; 
  signal N_PZ_122_MC_D_226 : STD_LOGIC; 
  signal N_PZ_122_MC_D1_227 : STD_LOGIC; 
  signal N_PZ_122_MC_D2_228 : STD_LOGIC; 
  signal N_PZ_129_MC_Q_229 : STD_LOGIC; 
  signal N_PZ_129_MC_D_230 : STD_LOGIC; 
  signal N_PZ_129_MC_D1_231 : STD_LOGIC; 
  signal N_PZ_129_MC_D2_232 : STD_LOGIC; 
  signal N_PZ_193_MC_Q_233 : STD_LOGIC; 
  signal N_PZ_193_MC_D_234 : STD_LOGIC; 
  signal N_PZ_193_MC_D1_235 : STD_LOGIC; 
  signal N_PZ_193_MC_D2_236 : STD_LOGIC; 
  signal N_PZ_193_MC_D2_PT_0_237 : STD_LOGIC; 
  signal N_PZ_193_MC_D2_PT_1_238 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D_240 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D1_241 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_242 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_0_243 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_1_244 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_2_245 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_D_247 : STD_LOGIC; 
  signal cnt_11_MC_D1_248 : STD_LOGIC; 
  signal cnt_11_MC_D2_249 : STD_LOGIC; 
  signal N_PZ_184_251 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_252 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_253 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_254 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_255 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_4_256 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_5_257 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_6_258 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_7_259 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D_261 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D1_262 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_263 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_0_264 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_1_265 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_2_266 : STD_LOGIC; 
  signal N_PZ_184_MC_Q_267 : STD_LOGIC; 
  signal N_PZ_184_MC_D_268 : STD_LOGIC; 
  signal N_PZ_184_MC_D1_269 : STD_LOGIC; 
  signal N_PZ_184_MC_D2_270 : STD_LOGIC; 
  signal N_PZ_184_MC_D2_PT_0_271 : STD_LOGIC; 
  signal N_PZ_184_MC_D2_PT_1_272 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_274 : STD_LOGIC; 
  signal cnt_1_MC_D1_275 : STD_LOGIC; 
  signal cnt_1_MC_D2_276 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_277 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_278 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_280 : STD_LOGIC; 
  signal cnt_2_MC_D1_281 : STD_LOGIC; 
  signal cnt_2_MC_D2_282 : STD_LOGIC; 
  signal N_PZ_154_283 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_284 : STD_LOGIC; 
  signal N_PZ_132_285 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_286 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_287 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_288 : STD_LOGIC; 
  signal N_PZ_132_MC_Q_289 : STD_LOGIC; 
  signal N_PZ_132_MC_D_290 : STD_LOGIC; 
  signal N_PZ_132_MC_D1_291 : STD_LOGIC; 
  signal N_PZ_132_MC_D2_292 : STD_LOGIC; 
  signal N_PZ_132_MC_D2_PT_0_293 : STD_LOGIC; 
  signal N_PZ_132_MC_D2_PT_1_294 : STD_LOGIC; 
  signal N_PZ_132_MC_D2_PT_2_295 : STD_LOGIC; 
  signal N_PZ_132_MC_D2_PT_3_296 : STD_LOGIC; 
  signal N_PZ_154_MC_Q_297 : STD_LOGIC; 
  signal N_PZ_154_MC_D_298 : STD_LOGIC; 
  signal N_PZ_154_MC_D1_299 : STD_LOGIC; 
  signal N_PZ_154_MC_D2_300 : STD_LOGIC; 
  signal N_PZ_154_MC_D2_PT_0_301 : STD_LOGIC; 
  signal N_PZ_154_MC_D2_PT_1_302 : STD_LOGIC; 
  signal N_PZ_154_MC_D2_PT_2_303 : STD_LOGIC; 
  signal N_PZ_154_MC_D2_PT_3_304 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q_305 : STD_LOGIC; 
  signal cnt_3_MC_D_306 : STD_LOGIC; 
  signal cnt_3_MC_D1_307 : STD_LOGIC; 
  signal cnt_3_MC_D2_308 : STD_LOGIC; 
  signal cnt_3_BUFR_309 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D_311 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D1_312 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_313 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_0_314 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_1_315 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_2_316 : STD_LOGIC; 
  signal cnt_3_BUFR_MC_D2_PT_3_317 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_4_MC_D_319 : STD_LOGIC; 
  signal cnt_4_MC_D1_320 : STD_LOGIC; 
  signal cnt_4_MC_D2_321 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_0_322 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_1_323 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_2_324 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_3_325 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_4_326 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_5_MC_D_328 : STD_LOGIC; 
  signal cnt_5_MC_D1_329 : STD_LOGIC; 
  signal cnt_5_MC_D2_330 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_0_331 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_1_332 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_2_333 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_3_334 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_4_335 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_5_336 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_6_337 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_7_338 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_340 : STD_LOGIC; 
  signal cnt_6_MC_D1_341 : STD_LOGIC; 
  signal cnt_6_MC_D2_342 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_343 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_344 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_2_345 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_3_346 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_4_347 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_7_MC_D_349 : STD_LOGIC; 
  signal cnt_7_MC_D1_350 : STD_LOGIC; 
  signal cnt_7_MC_D2_351 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_0_352 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_1_353 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_2_354 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_3_355 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_4_356 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_5_357 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_6_358 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_8_MC_D_360 : STD_LOGIC; 
  signal cnt_8_MC_D1_361 : STD_LOGIC; 
  signal cnt_8_MC_D2_362 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_0_363 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_1_364 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_2_365 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_3_366 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_4_367 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_5_368 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_6_369 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_9_MC_D_371 : STD_LOGIC; 
  signal cnt_9_MC_D1_372 : STD_LOGIC; 
  signal cnt_9_MC_D2_373 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_0_374 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_1_375 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_2_376 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_3_377 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q_378 : STD_LOGIC; 
  signal did_0_MC_D_379 : STD_LOGIC; 
  signal did_0_MC_D1_380 : STD_LOGIC; 
  signal did_0_MC_D2_381 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q_382 : STD_LOGIC; 
  signal did_1_MC_D_383 : STD_LOGIC; 
  signal did_1_MC_D1_384 : STD_LOGIC; 
  signal did_1_MC_D2_385 : STD_LOGIC; 
  signal did_2_MC_Q_tsimrenamed_net_Q_386 : STD_LOGIC; 
  signal did_2_MC_D_387 : STD_LOGIC; 
  signal did_2_MC_D1_388 : STD_LOGIC; 
  signal did_2_MC_D2_389 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_UIM_391 : STD_LOGIC; 
  signal err_MC_D_392 : STD_LOGIC; 
  signal err_MC_D1_393 : STD_LOGIC; 
  signal err_MC_D2_394 : STD_LOGIC; 
  signal err_MC_D2_PT_0_395 : STD_LOGIC; 
  signal err_MC_D2_PT_1_396 : STD_LOGIC; 
  signal err_MC_D2_PT_2_397 : STD_LOGIC; 
  signal err_MC_D2_PT_3_398 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_153_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_5_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_188_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_127_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_127_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_127_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_127_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_127_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_127_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_116_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_116_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_116_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_116_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_116_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_116_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_122_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_122_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_122_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_122_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_122_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_122_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_129_MC_D1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_193_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_184_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_132_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_154_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_7_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_153_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_5_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_188_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_127_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_127_MC_D1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_116_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_116_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_116_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_116_MC_D1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_193_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_184_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_132_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_132_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_132_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_154_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_154_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_154_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_7_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal cnt_1_valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_5
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_7
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_9
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_11
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_13
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_15
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_17
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_19
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_21
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_23
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_25
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_27
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_29
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_35,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_37,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_39,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_41,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_43,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_45,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_47,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_49,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_51,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_53,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_55,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_57,
      O => cnt(9)
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_59,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_61,
      O => did(1)
    );
  did_2_Q : X_BUF
    port map (
      I => did_2_MC_Q_63,
      O => did(2)
    );
  err_66 : X_BUF
    port map (
      I => err_MC_Q_65,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_35
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_68
    );
  Vcc : X_ONE
    port map (
      O => Vcc_69
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_67
    );
  cnt_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D1_IN1,
      O => cnt_0_MC_D1_70
    );
  cnt_0_MC_D2 : X_ZERO
    port map (
      O => cnt_0_MC_D2_71
    );
  N_PZ_153 : X_BUF
    port map (
      I => N_PZ_153_MC_Q_73,
      O => N_PZ_153_72
    );
  N_PZ_153_MC_Q : X_BUF
    port map (
      I => N_PZ_153_MC_D_74,
      O => N_PZ_153_MC_Q_73
    );
  N_PZ_153_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_153_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_153_MC_D_IN1,
      O => N_PZ_153_MC_D_74
    );
  N_PZ_153_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_153_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_153_MC_D1_IN1,
      O => N_PZ_153_MC_D1_75
    );
  N_PZ_153_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_N_PZ_153_MC_D2_PT_0_IN2,
      O => N_PZ_153_MC_D2_PT_0_80
    );
  N_PZ_153_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_153_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN2,
      O => N_PZ_153_MC_D2_PT_1_81
    );
  N_PZ_153_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_153_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_153_MC_D2_IN1,
      O => N_PZ_153_MC_D2_76
    );
  cnt_1_down_cs : X_BUF
    port map (
      I => cnt_1_down_cs_MC_Q,
      O => cnt_1_down_cs_77
    );
  cnt_1_down_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_down_cs_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_down_cs_MC_Q
    );
  cnt_1_down_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_down_cs_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_down_cs_MC_D_IN1,
      O => cnt_1_down_cs_MC_D_83
    );
  cnt_1_down_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1,
      O => cnt_1_down_cs_MC_D1_84
    );
  cnt_1_down_cs_MC_D2 : X_ZERO
    port map (
      O => cnt_1_down_cs_MC_D2_85
    );
  cnt_1_up_cs : X_BUF
    port map (
      I => cnt_1_up_cs_MC_Q,
      O => cnt_1_up_cs_78
    );
  cnt_1_up_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_up_cs_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_up_cs_MC_Q
    );
  cnt_1_up_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_up_cs_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_up_cs_MC_D_IN1,
      O => cnt_1_up_cs_MC_D_87
    );
  cnt_1_up_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1,
      O => cnt_1_up_cs_MC_D1_88
    );
  cnt_1_up_cs_MC_D2 : X_ZERO
    port map (
      O => cnt_1_up_cs_MC_D2_89
    );
  cnt_1_valLd_cs_0_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_Q,
      O => cnt_1_valLd_cs(0)
    );
  cnt_1_valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_0_MC_Q
    );
  cnt_1_valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1,
      O => cnt_1_valLd_cs_0_MC_D_91
    );
  cnt_1_valLd_cs_0_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_0_MC_D1_92
    );
  cnt_1_valLd_cs_0_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_0_MC_D2_PT_0_94
    );
  cnt_1_valLd_cs_0_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_0_MC_D2_PT_1_95
    );
  cnt_1_valLd_cs_0_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1,
      O => cnt_1_valLd_cs_0_MC_D2_93
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_Q_37
    );
  cnt_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_10_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_10_MC_Q_tsimrenamed_net_Q
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_97
    );
  cnt_10_MC_D1 : X_ZERO
    port map (
      O => cnt_10_MC_D1_98
    );
  cnt_10_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1,
      O => cnt_10_MC_D2_PT_0_101
    );
  cnt_10_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1,
      O => cnt_10_MC_D2_PT_1_103
    );
  cnt_10_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2,
      O => cnt_10_MC_D2_PT_2_105
    );
  cnt_10_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3,
      O => cnt_10_MC_D2_PT_3_107
    );
  cnt_10_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN5,
      O => cnt_10_MC_D2_PT_4_111
    );
  cnt_10_MC_D2_PT_5 : X_AND7
    port map (
      I0 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN5,
      I6 => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN6,
      O => cnt_10_MC_D2_PT_5_112
    );
  cnt_10_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_10_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_IN5,
      O => cnt_10_MC_D2_99
    );
  cnt_1_valLd_cs_10_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_Q,
      O => cnt_1_valLd_cs(10)
    );
  cnt_1_valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_10_MC_Q
    );
  cnt_1_valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1,
      O => cnt_1_valLd_cs_10_MC_D_114
    );
  cnt_1_valLd_cs_10_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_10_MC_D1_115
    );
  cnt_1_valLd_cs_10_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN2,
      O => cnt_1_valLd_cs_10_MC_D2_PT_0_117
    );
  cnt_1_valLd_cs_10_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_10_MC_D2_PT_1_118
    );
  cnt_1_valLd_cs_10_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_10_MC_D2_PT_2_119
    );
  cnt_1_valLd_cs_10_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2,
      O => cnt_1_valLd_cs_10_MC_D2_116
    );
  N_PZ_188 : X_BUF
    port map (
      I => N_PZ_188_MC_Q_120,
      O => N_PZ_188_102
    );
  N_PZ_188_MC_Q : X_BUF
    port map (
      I => N_PZ_188_MC_D_121,
      O => N_PZ_188_MC_Q_120
    );
  N_PZ_188_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_188_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_188_MC_D_IN1,
      O => N_PZ_188_MC_D_121
    );
  N_PZ_188_MC_D1 : X_ZERO
    port map (
      O => N_PZ_188_MC_D1_122
    );
  N_PZ_188_MC_D2_PT_0 : X_AND7
    port map (
      I0 => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_188_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN4,
      I5 => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN5,
      I6 => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN6,
      O => N_PZ_188_MC_D2_PT_0_125
    );
  N_PZ_188_MC_D2_PT_1 : X_AND7
    port map (
      I0 => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN6,
      O => N_PZ_188_MC_D2_PT_1_126
    );
  N_PZ_188_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_188_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_188_MC_D2_IN1,
      O => N_PZ_188_MC_D2_123
    );
  cnt_1_valLd_cs_1_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_Q,
      O => cnt_1_valLd_cs(1)
    );
  cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_129
    );
  cnt_1_valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_1_MC_Q
    );
  cnt_1_valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1,
      O => cnt_1_valLd_cs_1_MC_D_128
    );
  cnt_1_valLd_cs_1_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_1_MC_D1_130
    );
  cnt_1_valLd_cs_1_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_1_MC_D2_PT_0_132
    );
  cnt_1_valLd_cs_1_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_1_MC_D2_PT_1_133
    );
  cnt_1_valLd_cs_1_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_1_MC_D2_PT_2_134
    );
  cnt_1_valLd_cs_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_PT_3_135
    );
  cnt_1_valLd_cs_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_PT_4_136
    );
  cnt_1_valLd_cs_1_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_131
    );
  cnt_1_valLd_cs_8_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_Q,
      O => cnt_1_valLd_cs(8)
    );
  cnt_1_valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_8_MC_Q
    );
  cnt_1_valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1,
      O => cnt_1_valLd_cs_8_MC_D_138
    );
  cnt_1_valLd_cs_8_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_8_MC_D1_139
    );
  cnt_1_valLd_cs_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_8_MC_D2_PT_0_141
    );
  cnt_1_valLd_cs_8_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1,
      O => cnt_1_valLd_cs_8_MC_D2_PT_1_142
    );
  cnt_1_valLd_cs_8_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_8_MC_D2_PT_2_143
    );
  cnt_1_valLd_cs_8_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3,
      O => cnt_1_valLd_cs_8_MC_D2_PT_3_144
    );
  cnt_1_valLd_cs_8_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3,
      O => cnt_1_valLd_cs_8_MC_D2_PT_4_145
    );
  cnt_1_valLd_cs_8_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN3,
      O => cnt_1_valLd_cs_8_MC_D2_PT_5_146
    );
  cnt_1_valLd_cs_8_MC_D2_PT_6 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3,
      O => cnt_1_valLd_cs_8_MC_D2_PT_6_147
    );
  cnt_1_valLd_cs_8_MC_D2_PT_7 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN3,
      I4 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN5,
      O => cnt_1_valLd_cs_8_MC_D2_PT_7_148
    );
  cnt_1_valLd_cs_8_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN7,
      O => cnt_1_valLd_cs_8_MC_D2_140
    );
  N_PZ_127 : X_BUF
    port map (
      I => N_PZ_127_MC_Q_149,
      O => N_PZ_127_109
    );
  N_PZ_127_MC_Q : X_BUF
    port map (
      I => N_PZ_127_MC_D_150,
      O => N_PZ_127_MC_Q_149
    );
  N_PZ_127_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_127_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_127_MC_D_IN1,
      O => N_PZ_127_MC_D_150
    );
  N_PZ_127_MC_D1 : X_AND4
    port map (
      I0 => NlwBufferSignal_N_PZ_127_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_127_MC_D1_IN1,
      I2 => NlwBufferSignal_N_PZ_127_MC_D1_IN2,
      I3 => NlwInverterSignal_N_PZ_127_MC_D1_IN3,
      O => N_PZ_127_MC_D1_151
    );
  N_PZ_127_MC_D2 : X_ZERO
    port map (
      O => N_PZ_127_MC_D2_152
    );
  cnt_1_valLd_cs_6_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_Q,
      O => cnt_1_valLd_cs(6)
    );
  cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_158
    );
  cnt_1_valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_6_MC_Q
    );
  cnt_1_valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1,
      O => cnt_1_valLd_cs_6_MC_D_157
    );
  cnt_1_valLd_cs_6_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_6_MC_D1_159
    );
  cnt_1_valLd_cs_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_6_MC_D2_PT_0_161
    );
  cnt_1_valLd_cs_6_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_6_MC_D2_PT_1_162
    );
  cnt_1_valLd_cs_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_6_MC_D2_PT_2_163
    );
  cnt_1_valLd_cs_6_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6,
      O => cnt_1_valLd_cs_6_MC_D2_PT_3_164
    );
  cnt_1_valLd_cs_6_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_6_MC_D2_PT_4_169
    );
  cnt_1_valLd_cs_6_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4,
      O => cnt_1_valLd_cs_6_MC_D2_160
    );
  cnt_1_valLd_cs_2_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_Q,
      O => cnt_1_valLd_cs(2)
    );
  cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_172
    );
  cnt_1_valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_2_MC_Q
    );
  cnt_1_valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1,
      O => cnt_1_valLd_cs_2_MC_D_171
    );
  cnt_1_valLd_cs_2_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_2_MC_D1_173
    );
  cnt_1_valLd_cs_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_2_MC_D2_PT_0_175
    );
  cnt_1_valLd_cs_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_2_MC_D2_PT_1_176
    );
  cnt_1_valLd_cs_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_2_MC_D2_PT_2_177
    );
  cnt_1_valLd_cs_2_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5,
      O => cnt_1_valLd_cs_2_MC_D2_PT_3_178
    );
  cnt_1_valLd_cs_2_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5,
      O => cnt_1_valLd_cs_2_MC_D2_PT_4_179
    );
  cnt_1_valLd_cs_2_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4,
      O => cnt_1_valLd_cs_2_MC_D2_174
    );
  cnt_1_valLd_cs_3_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_Q,
      O => cnt_1_valLd_cs(3)
    );
  cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_182
    );
  cnt_1_valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_3_MC_Q
    );
  cnt_1_valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1,
      O => cnt_1_valLd_cs_3_MC_D_181
    );
  cnt_1_valLd_cs_3_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_3_MC_D1_183
    );
  cnt_1_valLd_cs_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_3_MC_D2_PT_0_185
    );
  cnt_1_valLd_cs_3_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_3_MC_D2_PT_1_186
    );
  cnt_1_valLd_cs_3_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_3_MC_D2_PT_2_187
    );
  cnt_1_valLd_cs_3_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6,
      O => cnt_1_valLd_cs_3_MC_D2_PT_3_188
    );
  cnt_1_valLd_cs_3_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6,
      O => cnt_1_valLd_cs_3_MC_D2_PT_4_189
    );
  cnt_1_valLd_cs_3_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4,
      O => cnt_1_valLd_cs_3_MC_D2_184
    );
  cnt_1_valLd_cs_4_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_Q,
      O => cnt_1_valLd_cs(4)
    );
  cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_192
    );
  cnt_1_valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_4_MC_Q
    );
  cnt_1_valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1,
      O => cnt_1_valLd_cs_4_MC_D_191
    );
  cnt_1_valLd_cs_4_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_4_MC_D1_193
    );
  cnt_1_valLd_cs_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_4_MC_D2_PT_0_195
    );
  cnt_1_valLd_cs_4_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_4_MC_D2_PT_1_196
    );
  cnt_1_valLd_cs_4_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_4_MC_D2_PT_2_197
    );
  cnt_1_valLd_cs_4_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7,
      O => cnt_1_valLd_cs_4_MC_D2_PT_3_198
    );
  cnt_1_valLd_cs_4_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7,
      O => cnt_1_valLd_cs_4_MC_D2_PT_4_199
    );
  cnt_1_valLd_cs_4_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4,
      O => cnt_1_valLd_cs_4_MC_D2_194
    );
  cnt_1_valLd_cs_5_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_Q,
      O => cnt_1_valLd_cs(5)
    );
  cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_202
    );
  cnt_1_valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_5_MC_Q
    );
  cnt_1_valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1,
      O => cnt_1_valLd_cs_5_MC_D_201
    );
  cnt_1_valLd_cs_5_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_5_MC_D1_203
    );
  cnt_1_valLd_cs_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_5_MC_D2_PT_0_205
    );
  cnt_1_valLd_cs_5_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_5_MC_D2_PT_1_206
    );
  cnt_1_valLd_cs_5_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_5_MC_D2_PT_2_207
    );
  cnt_1_valLd_cs_5_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_5_MC_D2_PT_3_208
    );
  cnt_1_valLd_cs_5_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_5_MC_D2_PT_4_209
    );
  cnt_1_valLd_cs_5_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4,
      O => cnt_1_valLd_cs_5_MC_D2_204
    );
  N_PZ_116 : X_BUF
    port map (
      I => N_PZ_116_MC_Q_210,
      O => N_PZ_116_154
    );
  N_PZ_116_MC_Q : X_BUF
    port map (
      I => N_PZ_116_MC_D_211,
      O => N_PZ_116_MC_Q_210
    );
  N_PZ_116_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_116_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_116_MC_D_IN1,
      O => N_PZ_116_MC_D_211
    );
  N_PZ_116_MC_D1 : X_AND4
    port map (
      I0 => NlwInverterSignal_N_PZ_116_MC_D1_IN0,
      I1 => NlwInverterSignal_N_PZ_116_MC_D1_IN1,
      I2 => NlwInverterSignal_N_PZ_116_MC_D1_IN2,
      I3 => NlwInverterSignal_N_PZ_116_MC_D1_IN3,
      O => N_PZ_116_MC_D1_212
    );
  N_PZ_116_MC_D2 : X_ZERO
    port map (
      O => N_PZ_116_MC_D2_213
    );
  cnt_1_valLd_cs_7_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_Q,
      O => cnt_1_valLd_cs(7)
    );
  cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_216
    );
  cnt_1_valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_7_MC_Q
    );
  cnt_1_valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1,
      O => cnt_1_valLd_cs_7_MC_D_215
    );
  cnt_1_valLd_cs_7_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_7_MC_D1_217
    );
  cnt_1_valLd_cs_7_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_7_MC_D2_PT_0_219
    );
  cnt_1_valLd_cs_7_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_7_MC_D2_PT_1_220
    );
  cnt_1_valLd_cs_7_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_7_MC_D2_PT_2_221
    );
  cnt_1_valLd_cs_7_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7,
      O => cnt_1_valLd_cs_7_MC_D2_PT_3_222
    );
  cnt_1_valLd_cs_7_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7,
      O => cnt_1_valLd_cs_7_MC_D2_PT_4_224
    );
  cnt_1_valLd_cs_7_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4,
      O => cnt_1_valLd_cs_7_MC_D2_218
    );
  N_PZ_122 : X_BUF
    port map (
      I => N_PZ_122_MC_Q_225,
      O => N_PZ_122_223
    );
  N_PZ_122_MC_Q : X_BUF
    port map (
      I => N_PZ_122_MC_D_226,
      O => N_PZ_122_MC_Q_225
    );
  N_PZ_122_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_122_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_122_MC_D_IN1,
      O => N_PZ_122_MC_D_226
    );
  N_PZ_122_MC_D1 : X_AND4
    port map (
      I0 => NlwBufferSignal_N_PZ_122_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_122_MC_D1_IN1,
      I2 => NlwBufferSignal_N_PZ_122_MC_D1_IN2,
      I3 => NlwBufferSignal_N_PZ_122_MC_D1_IN3,
      O => N_PZ_122_MC_D1_227
    );
  N_PZ_122_MC_D2 : X_ZERO
    port map (
      O => N_PZ_122_MC_D2_228
    );
  N_PZ_129 : X_BUF
    port map (
      I => N_PZ_129_MC_Q_229,
      O => N_PZ_129_124
    );
  N_PZ_129_MC_Q : X_BUF
    port map (
      I => N_PZ_129_MC_D_230,
      O => N_PZ_129_MC_Q_229
    );
  N_PZ_129_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_129_MC_D_IN1,
      O => N_PZ_129_MC_D_230
    );
  N_PZ_129_MC_D1 : X_AND4
    port map (
      I0 => NlwBufferSignal_N_PZ_129_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_129_MC_D1_IN1,
      I2 => NlwBufferSignal_N_PZ_129_MC_D1_IN2,
      I3 => NlwBufferSignal_N_PZ_129_MC_D1_IN3,
      O => N_PZ_129_MC_D1_231
    );
  N_PZ_129_MC_D2 : X_ZERO
    port map (
      O => N_PZ_129_MC_D2_232
    );
  N_PZ_193 : X_BUF
    port map (
      I => N_PZ_193_MC_Q_233,
      O => N_PZ_193_104
    );
  N_PZ_193_MC_Q : X_BUF
    port map (
      I => N_PZ_193_MC_D_234,
      O => N_PZ_193_MC_Q_233
    );
  N_PZ_193_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_193_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_193_MC_D_IN1,
      O => N_PZ_193_MC_D_234
    );
  N_PZ_193_MC_D1 : X_ZERO
    port map (
      O => N_PZ_193_MC_D1_235
    );
  N_PZ_193_MC_D2_PT_0 : X_AND6
    port map (
      I0 => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_193_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN4,
      I5 => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN5,
      O => N_PZ_193_MC_D2_PT_0_237
    );
  N_PZ_193_MC_D2_PT_1 : X_AND6
    port map (
      I0 => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN5,
      O => N_PZ_193_MC_D2_PT_1_238
    );
  N_PZ_193_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_193_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_193_MC_D2_IN1,
      O => N_PZ_193_MC_D2_236
    );
  cnt_1_valLd_cs_9_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_Q,
      O => cnt_1_valLd_cs(9)
    );
  cnt_1_valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_9_MC_Q
    );
  cnt_1_valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1,
      O => cnt_1_valLd_cs_9_MC_D_240
    );
  cnt_1_valLd_cs_9_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_9_MC_D1_241
    );
  cnt_1_valLd_cs_9_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN2,
      O => cnt_1_valLd_cs_9_MC_D2_PT_0_243
    );
  cnt_1_valLd_cs_9_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_9_MC_D2_PT_1_244
    );
  cnt_1_valLd_cs_9_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_9_MC_D2_PT_2_245
    );
  cnt_1_valLd_cs_9_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2,
      O => cnt_1_valLd_cs_9_MC_D2_242
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_39
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_247
    );
  cnt_11_MC_D1 : X_ZERO
    port map (
      O => cnt_11_MC_D1_248
    );
  cnt_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      O => cnt_11_MC_D2_PT_0_252
    );
  cnt_11_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3,
      O => cnt_11_MC_D2_PT_1_253
    );
  cnt_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => cnt_11_MC_D2_PT_2_254
    );
  cnt_11_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3,
      O => cnt_11_MC_D2_PT_3_255
    );
  cnt_11_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3,
      O => cnt_11_MC_D2_PT_4_256
    );
  cnt_11_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3,
      O => cnt_11_MC_D2_PT_5_257
    );
  cnt_11_MC_D2_PT_6 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3,
      O => cnt_11_MC_D2_PT_6_258
    );
  cnt_11_MC_D2_PT_7 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN3,
      O => cnt_11_MC_D2_PT_7_259
    );
  cnt_11_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_IN7,
      O => cnt_11_MC_D2_249
    );
  cnt_1_valLd_cs_11_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_Q,
      O => cnt_1_valLd_cs(11)
    );
  cnt_1_valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_11_MC_Q
    );
  cnt_1_valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1,
      O => cnt_1_valLd_cs_11_MC_D_261
    );
  cnt_1_valLd_cs_11_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_11_MC_D1_262
    );
  cnt_1_valLd_cs_11_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN2,
      O => cnt_1_valLd_cs_11_MC_D2_PT_0_264
    );
  cnt_1_valLd_cs_11_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_11_MC_D2_PT_1_265
    );
  cnt_1_valLd_cs_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_11_MC_D2_PT_2_266
    );
  cnt_1_valLd_cs_11_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2,
      O => cnt_1_valLd_cs_11_MC_D2_263
    );
  N_PZ_184 : X_BUF
    port map (
      I => N_PZ_184_MC_Q_267,
      O => N_PZ_184_251
    );
  N_PZ_184_MC_Q : X_BUF
    port map (
      I => N_PZ_184_MC_D_268,
      O => N_PZ_184_MC_Q_267
    );
  N_PZ_184_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_184_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_184_MC_D_IN1,
      O => N_PZ_184_MC_D_268
    );
  N_PZ_184_MC_D1 : X_ZERO
    port map (
      O => N_PZ_184_MC_D1_269
    );
  N_PZ_184_MC_D2_PT_0 : X_AND8
    port map (
      I0 => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_184_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN4,
      I5 => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN5,
      I6 => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN6,
      I7 => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN7,
      O => N_PZ_184_MC_D2_PT_0_271
    );
  N_PZ_184_MC_D2_PT_1 : X_AND8
    port map (
      I0 => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN6,
      I7 => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN7,
      O => N_PZ_184_MC_D2_PT_1_272
    );
  N_PZ_184_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_184_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_184_MC_D2_IN1,
      O => N_PZ_184_MC_D2_270
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_41
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_274
    );
  cnt_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D1_IN1,
      O => cnt_1_MC_D1_275
    );
  cnt_1_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3,
      O => cnt_1_MC_D2_PT_0_277
    );
  cnt_1_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3,
      O => cnt_1_MC_D2_PT_1_278
    );
  cnt_1_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      O => cnt_1_MC_D2_276
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_43
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_280
    );
  cnt_2_MC_D1 : X_ZERO
    port map (
      O => cnt_2_MC_D1_281
    );
  cnt_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      O => cnt_2_MC_D2_PT_0_284
    );
  cnt_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => cnt_2_MC_D2_PT_1_286
    );
  cnt_2_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2,
      O => cnt_2_MC_D2_PT_2_287
    );
  cnt_2_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3,
      O => cnt_2_MC_D2_PT_3_288
    );
  cnt_2_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      O => cnt_2_MC_D2_282
    );
  N_PZ_132 : X_BUF
    port map (
      I => N_PZ_132_MC_Q_289,
      O => N_PZ_132_285
    );
  N_PZ_132_MC_Q : X_BUF
    port map (
      I => N_PZ_132_MC_D_290,
      O => N_PZ_132_MC_Q_289
    );
  N_PZ_132_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_132_MC_D_IN1,
      O => N_PZ_132_MC_D_290
    );
  N_PZ_132_MC_D1 : X_ZERO
    port map (
      O => N_PZ_132_MC_D1_291
    );
  N_PZ_132_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN2,
      O => N_PZ_132_MC_D2_PT_0_293
    );
  N_PZ_132_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_132_MC_D2_PT_1_IN2,
      O => N_PZ_132_MC_D2_PT_1_294
    );
  N_PZ_132_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_132_MC_D2_PT_2_IN2,
      O => N_PZ_132_MC_D2_PT_2_295
    );
  N_PZ_132_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_N_PZ_132_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN2,
      O => N_PZ_132_MC_D2_PT_3_296
    );
  N_PZ_132_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_N_PZ_132_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_132_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_132_MC_D2_IN2,
      I3 => NlwBufferSignal_N_PZ_132_MC_D2_IN3,
      O => N_PZ_132_MC_D2_292
    );
  N_PZ_154 : X_BUF
    port map (
      I => N_PZ_154_MC_Q_297,
      O => N_PZ_154_283
    );
  N_PZ_154_MC_Q : X_BUF
    port map (
      I => N_PZ_154_MC_D_298,
      O => N_PZ_154_MC_Q_297
    );
  N_PZ_154_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_154_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_154_MC_D_IN1,
      O => N_PZ_154_MC_D_298
    );
  N_PZ_154_MC_D1 : X_ZERO
    port map (
      O => N_PZ_154_MC_D1_299
    );
  N_PZ_154_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN2,
      O => N_PZ_154_MC_D2_PT_0_301
    );
  N_PZ_154_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_154_MC_D2_PT_1_IN2,
      O => N_PZ_154_MC_D2_PT_1_302
    );
  N_PZ_154_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_N_PZ_154_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN2,
      O => N_PZ_154_MC_D2_PT_2_303
    );
  N_PZ_154_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_N_PZ_154_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN2,
      O => N_PZ_154_MC_D2_PT_3_304
    );
  N_PZ_154_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_N_PZ_154_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_154_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_154_MC_D2_IN2,
      I3 => NlwBufferSignal_N_PZ_154_MC_D2_IN3,
      O => N_PZ_154_MC_D2_300
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q_305,
      O => cnt_3_MC_Q_45
    );
  cnt_3_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_3_MC_D_306,
      O => cnt_3_MC_Q_tsimrenamed_net_Q_305
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_306
    );
  cnt_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D1_IN1,
      O => cnt_3_MC_D1_307
    );
  cnt_3_MC_D2 : X_ZERO
    port map (
      O => cnt_3_MC_D2_308
    );
  cnt_3_BUFR : X_BUF
    port map (
      I => cnt_3_BUFR_MC_Q,
      O => cnt_3_BUFR_309
    );
  cnt_3_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_3_BUFR_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_3_BUFR_MC_Q
    );
  cnt_3_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D_IN1,
      O => cnt_3_BUFR_MC_D_311
    );
  cnt_3_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_3_BUFR_MC_D1_312
    );
  cnt_3_BUFR_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN1,
      O => cnt_3_BUFR_MC_D2_PT_0_314
    );
  cnt_3_BUFR_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN3,
      O => cnt_3_BUFR_MC_D2_PT_1_315
    );
  cnt_3_BUFR_MC_D2_PT_2 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN6,
      O => cnt_3_BUFR_MC_D2_PT_2_316
    );
  cnt_3_BUFR_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN6,
      O => cnt_3_BUFR_MC_D2_PT_3_317
    );
  cnt_3_BUFR_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN3,
      O => cnt_3_BUFR_MC_D2_313
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_Q_47
    );
  cnt_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_4_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_4_MC_Q_tsimrenamed_net_Q
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_319
    );
  cnt_4_MC_D1 : X_ZERO
    port map (
      O => cnt_4_MC_D1_320
    );
  cnt_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1,
      O => cnt_4_MC_D2_PT_0_322
    );
  cnt_4_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3,
      O => cnt_4_MC_D2_PT_1_323
    );
  cnt_4_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3,
      O => cnt_4_MC_D2_PT_2_324
    );
  cnt_4_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN7,
      O => cnt_4_MC_D2_PT_3_325
    );
  cnt_4_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6,
      I7 => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7,
      O => cnt_4_MC_D2_PT_4_326
    );
  cnt_4_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_IN4,
      O => cnt_4_MC_D2_321
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_Q_49
    );
  cnt_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_5_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_5_MC_Q_tsimrenamed_net_Q
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_328
    );
  cnt_5_MC_D1 : X_ZERO
    port map (
      O => cnt_5_MC_D1_329
    );
  cnt_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1,
      O => cnt_5_MC_D2_PT_0_331
    );
  cnt_5_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3,
      O => cnt_5_MC_D2_PT_1_332
    );
  cnt_5_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3,
      O => cnt_5_MC_D2_PT_2_333
    );
  cnt_5_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3,
      O => cnt_5_MC_D2_PT_3_334
    );
  cnt_5_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3,
      O => cnt_5_MC_D2_PT_4_335
    );
  cnt_5_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3,
      O => cnt_5_MC_D2_PT_5_336
    );
  cnt_5_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN5,
      O => cnt_5_MC_D2_PT_6_337
    );
  cnt_5_MC_D2_PT_7 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN7,
      I8 => NlwInverterSignal_cnt_5_MC_D2_PT_7_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN15,
      O => cnt_5_MC_D2_PT_7_338
    );
  cnt_5_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_IN7,
      O => cnt_5_MC_D2_330
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_51
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_340
    );
  cnt_6_MC_D1 : X_ZERO
    port map (
      O => cnt_6_MC_D1_341
    );
  cnt_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      O => cnt_6_MC_D2_PT_0_343
    );
  cnt_6_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3,
      O => cnt_6_MC_D2_PT_1_344
    );
  cnt_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN3,
      O => cnt_6_MC_D2_PT_2_345
    );
  cnt_6_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3,
      O => cnt_6_MC_D2_PT_3_346
    );
  cnt_6_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6,
      O => cnt_6_MC_D2_PT_4_347
    );
  cnt_6_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_IN4,
      O => cnt_6_MC_D2_342
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_Q_53
    );
  cnt_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_7_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_7_MC_Q_tsimrenamed_net_Q
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_349
    );
  cnt_7_MC_D1 : X_ZERO
    port map (
      O => cnt_7_MC_D1_350
    );
  cnt_7_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1,
      O => cnt_7_MC_D2_PT_0_352
    );
  cnt_7_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2,
      O => cnt_7_MC_D2_PT_1_353
    );
  cnt_7_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3,
      O => cnt_7_MC_D2_PT_2_354
    );
  cnt_7_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3,
      O => cnt_7_MC_D2_PT_3_355
    );
  cnt_7_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3,
      O => cnt_7_MC_D2_PT_4_356
    );
  cnt_7_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN3,
      O => cnt_7_MC_D2_PT_5_357
    );
  cnt_7_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_7_MC_D2_PT_6_IN4,
      O => cnt_7_MC_D2_PT_6_358
    );
  cnt_7_MC_D2 : X_OR7
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_7_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_7_MC_D2_IN6,
      O => cnt_7_MC_D2_351
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_Q_55
    );
  cnt_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_8_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_8_MC_Q_tsimrenamed_net_Q
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_360
    );
  cnt_8_MC_D1 : X_ZERO
    port map (
      O => cnt_8_MC_D1_361
    );
  cnt_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1,
      O => cnt_8_MC_D2_PT_0_363
    );
  cnt_8_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN3,
      O => cnt_8_MC_D2_PT_1_364
    );
  cnt_8_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3,
      O => cnt_8_MC_D2_PT_2_365
    );
  cnt_8_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3,
      O => cnt_8_MC_D2_PT_3_366
    );
  cnt_8_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN3,
      O => cnt_8_MC_D2_PT_4_367
    );
  cnt_8_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_8_MC_D2_PT_5_IN4,
      I5 => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN5,
      O => cnt_8_MC_D2_PT_5_368
    );
  cnt_8_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN4,
      I5 => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN5,
      O => cnt_8_MC_D2_PT_6_369
    );
  cnt_8_MC_D2 : X_OR7
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_8_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_8_MC_D2_IN6,
      O => cnt_8_MC_D2_362
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_Q_57
    );
  cnt_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_9_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_9_MC_Q_tsimrenamed_net_Q
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_371
    );
  cnt_9_MC_D1 : X_ZERO
    port map (
      O => cnt_9_MC_D1_372
    );
  cnt_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN1,
      O => cnt_9_MC_D2_PT_0_374
    );
  cnt_9_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2,
      O => cnt_9_MC_D2_PT_1_375
    );
  cnt_9_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3,
      O => cnt_9_MC_D2_PT_2_376
    );
  cnt_9_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN5,
      O => cnt_9_MC_D2_PT_3_377
    );
  cnt_9_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_IN3,
      O => cnt_9_MC_D2_373
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q_378,
      O => did_0_MC_Q_59
    );
  did_0_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_0_MC_D_379,
      O => did_0_MC_Q_tsimrenamed_net_Q_378
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_379
    );
  did_0_MC_D1 : X_ZERO
    port map (
      O => did_0_MC_D1_380
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_381
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q_382,
      O => did_1_MC_Q_61
    );
  did_1_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_1_MC_D_383,
      O => did_1_MC_Q_tsimrenamed_net_Q_382
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_383
    );
  did_1_MC_D1 : X_ZERO
    port map (
      O => did_1_MC_D1_384
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_385
    );
  did_2_MC_Q : X_BUF
    port map (
      I => did_2_MC_Q_tsimrenamed_net_Q_386,
      O => did_2_MC_Q_63
    );
  did_2_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_2_MC_D_387,
      O => did_2_MC_Q_tsimrenamed_net_Q_386
    );
  did_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_2_MC_D_IN0,
      I1 => NlwBufferSignal_did_2_MC_D_IN1,
      O => did_2_MC_D_387
    );
  did_2_MC_D1 : X_ZERO
    port map (
      O => did_2_MC_D1_388
    );
  did_2_MC_D2 : X_ZERO
    port map (
      O => did_2_MC_D2_389
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_65
    );
  err_MC_UIM : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_UIM_391
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_392
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_393
    );
  err_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      O => err_MC_D2_PT_0_395
    );
  err_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_1_IN2,
      O => err_MC_D2_PT_1_396
    );
  err_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_2_IN7,
      I8 => NlwBufferSignal_err_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_2_IN15,
      O => err_MC_D2_PT_2_397
    );
  err_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_err_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_err_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_3_IN15,
      O => err_MC_D2_PT_3_398
    );
  err_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      O => err_MC_D2_394
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_67,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_70,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_71,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_0_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_153_MC_D1_75,
      O => NlwBufferSignal_N_PZ_153_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_153_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_153_MC_D2_76,
      O => NlwBufferSignal_N_PZ_153_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_153_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_N_PZ_153_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_153_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_153_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_153_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_153_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_153_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_153_MC_D2_PT_0_80,
      O => NlwBufferSignal_N_PZ_153_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_153_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_153_MC_D2_PT_1_81,
      O => NlwBufferSignal_N_PZ_153_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_down_cs_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D_83,
      O => NlwBufferSignal_cnt_1_down_cs_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_down_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D1_84,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D2_85,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_5,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_up_cs_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D_87,
      O => NlwBufferSignal_cnt_1_up_cs_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_up_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D1_88,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D2_89,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_7,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D_91,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D1_92,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_93,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_0_II_UIM_11,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_0_94,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_1_95,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_MC_D_97,
      O => NlwBufferSignal_cnt_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_98,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_99,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_5_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_0_101,
      O => NlwBufferSignal_cnt_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_1_103,
      O => NlwBufferSignal_cnt_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_2_105,
      O => NlwBufferSignal_cnt_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_3_107,
      O => NlwBufferSignal_cnt_10_MC_D2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_4_111,
      O => NlwBufferSignal_cnt_10_MC_D2_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_5_112,
      O => NlwBufferSignal_cnt_10_MC_D2_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D_114,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D1_115,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_116,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_10_II_UIM_13,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_0_117,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_1_118,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_2_119,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_188_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_188_MC_D1_122,
      O => NlwBufferSignal_N_PZ_188_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_188_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_188_MC_D2_123,
      O => NlwBufferSignal_N_PZ_188_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_N_PZ_188_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_N_PZ_188_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_188_MC_D2_PT_0_125,
      O => NlwBufferSignal_N_PZ_188_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_188_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_188_MC_D2_PT_1_126,
      O => NlwBufferSignal_N_PZ_188_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D_128,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_129,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D1_130,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_131,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_1_II_UIM_15,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_1_II_UIM_15,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_0_132,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_1_133,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_2_134,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_3_135,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_4_136,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D_138,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D1_139,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_140,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => valLd_8_II_UIM_17,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_0_141,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_1_142,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_2_143,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_3_144,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_4_145,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_5_146,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_6_147,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_7_148,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN7
    );
  NlwBufferBlock_N_PZ_127_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_127_MC_D1_151,
      O => NlwBufferSignal_N_PZ_127_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_127_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_127_MC_D2_152,
      O => NlwBufferSignal_N_PZ_127_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_127_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_N_PZ_127_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_127_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_N_PZ_127_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_127_MC_D1_IN2 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_N_PZ_127_MC_D1_IN2
    );
  NlwBufferBlock_N_PZ_127_MC_D1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_N_PZ_127_MC_D1_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D_157,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_158,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D1_159,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_160,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_6_II_UIM_27,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_6_II_UIM_27,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_0_161,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_1_162,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_2_163,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_3_164,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_4_169,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D_171,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_172,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D1_173,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_174,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_2_II_UIM_19,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_2_II_UIM_19,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_0_175,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_1_176,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_2_177,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_3_178,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_4_179,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D_181,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_182,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D1_183,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_184,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_3_II_UIM_21,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_3_II_UIM_21,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_0_185,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_1_186,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_2_187,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_3_188,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_4_189,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D_191,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_192,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D1_193,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_194,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_4_II_UIM_23,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_4_II_UIM_23,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_0_195,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_1_196,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_2_197,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_3_198,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_4_199,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D_201,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_202,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D1_203,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_204,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_5_II_UIM_25,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_5_II_UIM_25,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_0_205,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_1_206,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_2_207,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_3_208,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_4_209,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4
    );
  NlwBufferBlock_N_PZ_116_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_116_MC_D1_212,
      O => NlwBufferSignal_N_PZ_116_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_116_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_116_MC_D2_213,
      O => NlwBufferSignal_N_PZ_116_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_116_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_116_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_116_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_N_PZ_116_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_116_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_N_PZ_116_MC_D1_IN2
    );
  NlwBufferBlock_N_PZ_116_MC_D1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_N_PZ_116_MC_D1_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D_215,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_216,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D1_217,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_218,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_7_II_UIM_29,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_7_II_UIM_29,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_0_219,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_1_220,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_2_221,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_3_222,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_4_224,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4
    );
  NlwBufferBlock_N_PZ_122_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_122_MC_D1_227,
      O => NlwBufferSignal_N_PZ_122_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_122_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_122_MC_D2_228,
      O => NlwBufferSignal_N_PZ_122_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_122_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_N_PZ_122_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_122_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_N_PZ_122_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_122_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_N_PZ_122_MC_D1_IN2
    );
  NlwBufferBlock_N_PZ_122_MC_D1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_N_PZ_122_MC_D1_IN3
    );
  NlwBufferBlock_N_PZ_129_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_129_MC_D1_231,
      O => NlwBufferSignal_N_PZ_129_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_129_MC_D2_232,
      O => NlwBufferSignal_N_PZ_129_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_129_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_129_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_N_PZ_129_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_129_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_129_MC_D1_IN2
    );
  NlwBufferBlock_N_PZ_129_MC_D1_IN3 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_N_PZ_129_MC_D1_IN3
    );
  NlwBufferBlock_N_PZ_193_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_193_MC_D1_235,
      O => NlwBufferSignal_N_PZ_193_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_193_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_193_MC_D2_236,
      O => NlwBufferSignal_N_PZ_193_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_N_PZ_193_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_N_PZ_193_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_193_MC_D2_PT_0_237,
      O => NlwBufferSignal_N_PZ_193_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_193_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_193_MC_D2_PT_1_238,
      O => NlwBufferSignal_N_PZ_193_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D_240,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D1_241,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_242,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_9_II_UIM_31,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_0_243,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_1_244,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_2_245,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_D_247,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_248,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_249,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_184_251,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_184_251,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_188_102,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => N_PZ_184_251,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_252,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_253,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_254,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_255,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_4_256,
      O => NlwBufferSignal_cnt_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_5_257,
      O => NlwBufferSignal_cnt_11_MC_D2_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_6_258,
      O => NlwBufferSignal_cnt_11_MC_D2_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_7_259,
      O => NlwBufferSignal_cnt_11_MC_D2_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D_261,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D1_262,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_263,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_184_251,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_184_251,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_0_264,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_1_265,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_2_266,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_184_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_184_MC_D1_269,
      O => NlwBufferSignal_N_PZ_184_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_184_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_184_MC_D2_270,
      O => NlwBufferSignal_N_PZ_184_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_N_PZ_184_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_N_PZ_184_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_184_MC_D2_PT_0_271,
      O => NlwBufferSignal_N_PZ_184_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_184_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_184_MC_D2_PT_1_272,
      O => NlwBufferSignal_N_PZ_184_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_D_274,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_275,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_276,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_277,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_278,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_D_280,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_281,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_282,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_284,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_286,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_287,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_288,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_N_PZ_132_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_132_MC_D1_291,
      O => NlwBufferSignal_N_PZ_132_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_132_MC_D2_292,
      O => NlwBufferSignal_N_PZ_132_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_N_PZ_132_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_132_MC_D2_PT_0_293,
      O => NlwBufferSignal_N_PZ_132_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_132_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_132_MC_D2_PT_1_294,
      O => NlwBufferSignal_N_PZ_132_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_132_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_132_MC_D2_PT_2_295,
      O => NlwBufferSignal_N_PZ_132_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_132_MC_D2_IN3 : X_BUF
    port map (
      I => N_PZ_132_MC_D2_PT_3_296,
      O => NlwBufferSignal_N_PZ_132_MC_D2_IN3
    );
  NlwBufferBlock_N_PZ_154_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_154_MC_D1_299,
      O => NlwBufferSignal_N_PZ_154_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_154_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_154_MC_D2_300,
      O => NlwBufferSignal_N_PZ_154_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_N_PZ_154_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_N_PZ_154_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_154_MC_D2_PT_0_301,
      O => NlwBufferSignal_N_PZ_154_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_154_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_154_MC_D2_PT_1_302,
      O => NlwBufferSignal_N_PZ_154_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_154_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_154_MC_D2_PT_2_303,
      O => NlwBufferSignal_N_PZ_154_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_154_MC_D2_IN3 : X_BUF
    port map (
      I => N_PZ_154_MC_D2_PT_3_304,
      O => NlwBufferSignal_N_PZ_154_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_307,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_308,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_3_BUFR_309,
      O => NlwBufferSignal_cnt_3_MC_D1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_3_BUFR_309,
      O => NlwBufferSignal_cnt_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D_311,
      O => NlwBufferSignal_cnt_3_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D1_312,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_313,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_0_314,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_1_315,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_2_316,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_3_BUFR_MC_D2_PT_3_317,
      O => NlwBufferSignal_cnt_3_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_MC_D_319,
      O => NlwBufferSignal_cnt_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_320,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_321,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_0_322,
      O => NlwBufferSignal_cnt_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_1_323,
      O => NlwBufferSignal_cnt_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_2_324,
      O => NlwBufferSignal_cnt_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_3_325,
      O => NlwBufferSignal_cnt_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_4_326,
      O => NlwBufferSignal_cnt_4_MC_D2_IN4
    );
  NlwBufferBlock_cnt_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_MC_D_328,
      O => NlwBufferSignal_cnt_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_329,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_330,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_7_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_0_331,
      O => NlwBufferSignal_cnt_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_1_332,
      O => NlwBufferSignal_cnt_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_2_333,
      O => NlwBufferSignal_cnt_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_3_334,
      O => NlwBufferSignal_cnt_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_4_335,
      O => NlwBufferSignal_cnt_5_MC_D2_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_5_336,
      O => NlwBufferSignal_cnt_5_MC_D2_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_6_337,
      O => NlwBufferSignal_cnt_5_MC_D2_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_7_338,
      O => NlwBufferSignal_cnt_5_MC_D2_IN7
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_D_340,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_341,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_342,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_153_72,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_343,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_344,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_2_345,
      O => NlwBufferSignal_cnt_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_3_346,
      O => NlwBufferSignal_cnt_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_4_347,
      O => NlwBufferSignal_cnt_6_MC_D2_IN4
    );
  NlwBufferBlock_cnt_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_MC_D_349,
      O => NlwBufferSignal_cnt_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_350,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_351,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => N_PZ_154_283,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_0_352,
      O => NlwBufferSignal_cnt_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_1_353,
      O => NlwBufferSignal_cnt_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_2_354,
      O => NlwBufferSignal_cnt_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_3_355,
      O => NlwBufferSignal_cnt_7_MC_D2_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_4_356,
      O => NlwBufferSignal_cnt_7_MC_D2_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_5_357,
      O => NlwBufferSignal_cnt_7_MC_D2_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_6_358,
      O => NlwBufferSignal_cnt_7_MC_D2_IN6
    );
  NlwBufferBlock_cnt_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_MC_D_360,
      O => NlwBufferSignal_cnt_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_361,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_362,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_116_154,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_0_363,
      O => NlwBufferSignal_cnt_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_1_364,
      O => NlwBufferSignal_cnt_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_2_365,
      O => NlwBufferSignal_cnt_8_MC_D2_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_3_366,
      O => NlwBufferSignal_cnt_8_MC_D2_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_4_367,
      O => NlwBufferSignal_cnt_8_MC_D2_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_5_368,
      O => NlwBufferSignal_cnt_8_MC_D2_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_6_369,
      O => NlwBufferSignal_cnt_8_MC_D2_IN6
    );
  NlwBufferBlock_cnt_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_MC_D_371,
      O => NlwBufferSignal_cnt_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_372,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_373,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_193_104,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_122_223,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_132_285,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_0_374,
      O => NlwBufferSignal_cnt_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_1_375,
      O => NlwBufferSignal_cnt_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_2_376,
      O => NlwBufferSignal_cnt_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_3_377,
      O => NlwBufferSignal_cnt_9_MC_D2_IN3
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_380,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_381,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_384,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_385,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_did_2_MC_D_IN0 : X_BUF
    port map (
      I => did_2_MC_D1_388,
      O => NlwBufferSignal_did_2_MC_D_IN0
    );
  NlwBufferBlock_did_2_MC_D_IN1 : X_BUF
    port map (
      I => did_2_MC_D2_389,
      O => NlwBufferSignal_did_2_MC_D_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_392,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_393,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_394,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => err_MC_UIM_391,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_77,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => N_PZ_129_124,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_78,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => N_PZ_127_109,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_395,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_396,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_397,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_398,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwInverterBlock_N_PZ_153_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_N_PZ_153_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_N_PZ_153_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_153_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_153_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_10_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D_IN0,
      O => NlwInverterSignal_cnt_10_MC_D_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN0,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN0
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_5_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_5_IN6,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_5_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_N_PZ_188_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_188_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_188_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_188_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_188_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_188_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_N_PZ_188_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_N_PZ_188_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_188_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_N_PZ_188_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_7_IN4
    );
  NlwInverterBlock_N_PZ_127_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_127_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_127_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_127_MC_D1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_127_MC_D1_IN3,
      O => NlwInverterSignal_N_PZ_127_MC_D1_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_N_PZ_116_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_116_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_116_MC_D1_IN0
    );
  NlwInverterBlock_N_PZ_116_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_116_MC_D1_IN1,
      O => NlwInverterSignal_N_PZ_116_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_116_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_116_MC_D1_IN2,
      O => NlwInverterSignal_N_PZ_116_MC_D1_IN2
    );
  NlwInverterBlock_N_PZ_116_MC_D1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_116_MC_D1_IN3,
      O => NlwInverterSignal_N_PZ_116_MC_D1_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_N_PZ_193_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_193_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_193_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_193_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_193_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_193_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_N_PZ_193_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_193_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_N_PZ_193_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_N_PZ_184_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_184_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_184_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_184_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_184_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_184_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_N_PZ_184_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_N_PZ_184_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_N_PZ_184_MC_D2_PT_1_IN7 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_184_MC_D2_PT_1_IN7,
      O => NlwInverterSignal_N_PZ_184_MC_D2_PT_1_IN7
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_2_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D_IN0,
      O => NlwInverterSignal_cnt_2_MC_D_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_N_PZ_132_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_132_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_132_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_132_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_132_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_132_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_N_PZ_132_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_132_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_N_PZ_132_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_N_PZ_154_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_154_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_154_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_154_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_154_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_N_PZ_154_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_N_PZ_154_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_154_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_N_PZ_154_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_3_BUFR_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_BUFR_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_3_BUFR_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_4_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_4_IN7,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_4_IN7
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_7_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_7_IN8,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_7_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_4_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_4_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_4_IN6
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_cnt_9_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D_IN0,
      O => NlwInverterSignal_cnt_9_MC_D_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_did_1_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_did_1_MC_D_IN0,
      O => NlwInverterSignal_did_1_MC_D_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN8
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure_atgl;

