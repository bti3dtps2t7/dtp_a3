
library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;

entity rc4counter is
	port (
		clk : in std_logic;
		err_arop : in std_logic;
		cnt_arop : in std_logic_vector(11 downto 0);
		err_argl : in std_logic;
		cnt_argl : in std_logic_vector(11 downto 0);
		err_aril : in std_logic;
		cnt_aril : in std_logic_vector(11 downto 0);
		ok : out std_logic
		);
end entity rc4counter;


architecture beh of rc4counter is
signal ok_cs : std_logic := '0';
begin

checker:
process(clk) is

begin
	if clk = '1' and clk'event then
		if (err_arop /= err_aril) OR (err_arop /= err_argl) OR (err_argl /= err_aril) OR (cnt_arop /= cnt_aril) OR (cnt_arop /= cnt_argl) OR (cnt_argl /= cnt_aril) then
			ok_cs <= '0';
		else 
			ok_cs <= '1';
		end if;
	end if;

end process checker;

ok <= ok_cs;


end architecture beh;

