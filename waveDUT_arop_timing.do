onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb4countertime/ValLd_s
add wave -noupdate /tb4countertime/up_s
add wave -noupdate /tb4countertime/down_s
add wave -noupdate /tb4countertime/nLd_s
add wave -noupdate /tb4countertime/clk_s
add wave -noupdate /tb4countertime/nres_s
add wave -noupdate /tb4countertime/did_arop_s
add wave -noupdate /tb4countertime/err_arop_s
add wave -noupdate /tb4countertime/cnt_arop_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {365710 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 126
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1827 ns}
