

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;


architecture argl of counter is
signal cnt_ns : std_logic_vector(11 downto 0);
signal valLd_ns : std_logic_vector(11 downto 0);
signal err_ns : std_logic;
signal up_ns : std_logic;
signal down_ns : std_logic;

signal cnt_cs : std_logic_vector(11 downto 0):= (others=>'0');
signal valLd_cs : std_logic_vector(11 downto 0):= (others=>'0');
signal err_cs : std_logic := '0';
signal up_cs : std_logic := '0';
signal down_cs : std_logic := '0';



component fa is
    port (
        s  : out std_logic;
        co : out std_logic;
        x  : in  std_logic;
        y  : in  std_logic;
        ci : in  std_logic
    );--]port
end component fa;
for all : fa use entity work.fa( arc1 );


signal carry_s : std_logic_vector(10 downto 0);
signal y_s : std_logic;
signal ci_s : std_logic;
signal input_s : std_logic;

begin

up_ns <= up;
down_ns <= down;
valLd_ns <= valLd;

reg:
    process (clk) is
    variable err_flag : std_logic;    
    begin
        if clk = '1' and clk'event then
            if nres = '0'   then						 -- Reset
                cnt_cs <= (others=>'0');
                valLd_cs <= (others=>'0');
                err_cs <= '0';
                up_cs <= '0';
                down_cs <= '0';
            else 
                cnt_cs <= cnt_ns;
                err_flag := err_cs;

                if up_cs = '1' and down_cs = '0' then
                    err_flag := err_ns or err_flag;
                    err_cs <= err_flag;
                elsif up_cs = '0' and down_cs = '1' then
                    err_flag := (not err_ns) or err_flag;
                    err_cs <= err_flag;
                elsif up_cs = '1' and down_cs = '1' then
                    err_flag := '1';
                    err_cs <= err_flag;
                end if;

                if nLd = '0' then						-- Load new external value
                	valLd_cs <= valLd_ns;
                elsif nLd ='1' then					-- Keep counting with prior result
                	valLd_cs <= cnt_ns;
                end if;

                up_cs <= up_ns;
                down_cs <= down_ns;
            end if;
        end if;
    end process reg;


    process (up_cs, down_cs, valLd_cs) is
    begin

    if up_cs = '1' and down_cs = '0' then
        input_s <= up_cs;
        ci_s <= '0';
        y_s <= '0';
    elsif up_cs = '0' and down_cs = '1' then
        input_s <= not down_cs;
        ci_s <= '1';
        y_s <= '1';
    elsif up_cs = '0' and down_cs = '0' then
        input_s <= '0';
        ci_s <= '0';
        y_s <= '0';
    else
        input_s <= '0';
        ci_s <= '0';
        y_s <= '0';
    end if;

    end process;



		fa_add:
		for I in 0 to 11 generate
			head_a:
			if I = 0 generate
				fa_h : fa port map(x => valLd_cs(I), y => input_s, ci => ci_s,       s => cnt_ns(I), co => carry_s(I));
			end generate head_a;

			middle_a:
			if 0<I and I<11 generate
				fa_m : fa port map(x => valLd_cs(I), y => y_s,   ci => carry_s(I-1), s => cnt_ns(I), co => carry_s(I));
			end generate middle_a;

			tail_a:
			if I = 11 generate
				fa_t : fa port map(x => valLd_cs(I), y => y_s,   ci => carry_s(I-1), s => cnt_ns(I), co => err_ns);
			end generate tail_a;

		end generate fa_add;


cnt <= cnt_cs;
err <= err_cs;
did <= "010";


end architecture argl;



