-- Code belongs to TI3 DT
--
-- VCS: git@bitbucket.org/schaefers/DT-DEMO-SimpleGates.git
-- PUB: https://pub.informatik.haw-hamburg.de/home/pub/prof/schaefers_michael/*_DT/_CODE_/exampleSimpleGates/...
--
--
-- History:
-- ========
-- 111005 / WS11/12 :
--      1st version for TI3 DT WS11/12  by Michael Schaefers
-- WS14/15 (141104):
--      update for TI3 DT WS14/15  by Michael Schaefers
------------------------------------------------------------------------------
-- BEMERKUNG:
-- ==========
-- Deutscher Kommentar fuer Erklaerungen fuer speziell Studenten/VHDL-Anfaenger - dieser Kommentar wuerde im "Normalfall" fehlen
-- Englischer Kommentar als "normaler" Kommentar



library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic





-- Bemerkung(en):
-- ~~~~~~~~~~~~~~
--
-- Dies ist nur Beispiel-Code, der als einfache VHDL-Source-Code-Demo gelten soll.
-- Die "algorithmische Funktion" ist bewusst trivial, damit sich Studenten auf VHDL konzentrieren koennen.
-- Leider ist in der Konsequenz eine sinnvolle Benamung vereinzelt schwierig.
-- In einem "ernsthaften" Projekt sind Gatter KEINE ENTITYs.
-- 
-- Das folgende "delayVisualization"-GENERIC ist NUR fuer Anfaenger (um ein moegliches Problem mit Delta-Zyklen zu entschaerfen).
-- Vor der Synthese sind codierte DELAYs schlechter Stil.
-- DELAYs werden automatisch bei der Bachannotation eingeführt.
-- Es gilt die Regel: Ein RTL-Designer schreibt KEIN "after" (im RTL-VHDL-Code).
-- Von daher kann nachfolgend generic(...); gern weggelassen werden ;-)
-- Wenn die GENERICs (fuer delayVisualization )weggelassen werden, dann empfiehlt es sich, dies ueberall zu tun.
entity fa is
    port (
        s  : out std_logic := '0';
        co : out std_logic := '0';
        x  : in  std_logic;
        y  : in  std_logic;
        ci : in  std_logic
    );--]port
end entity fa;




-- single process (containing 2 "normal"/sequential signal assignments)
architecture arc1 of fa is                                  -- ACHTUNG: "arc?" ist eigentlich ein schlechter Name -> bessere Namen sind z.B. behavior, rtl, structure, ...
begin
    
    faLogic:
    process ( x, y, ci ) is
    begin
        s   <=  x xor y xor ci;
        co  <=  (x and y) or (x and ci) or (y and ci);
    end process faLogic;
     
end architecture arc1;
