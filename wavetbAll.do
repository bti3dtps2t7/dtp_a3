onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb4counter/ValLd_s
add wave -noupdate /tb4counter/up_s
add wave -noupdate /tb4counter/down_s
add wave -noupdate /tb4counter/nLd_s
add wave -noupdate /tb4counter/clk_s
add wave -noupdate /tb4counter/nres_s
add wave -noupdate -divider -height 50 arop
add wave -noupdate /tb4counter/did_arop_s
add wave -noupdate /tb4counter/err_arop_s
add wave -noupdate /tb4counter/cnt_arop_s
add wave -noupdate -divider -height 50 argl
add wave -noupdate /tb4counter/did_argl_s
add wave -noupdate /tb4counter/err_argl_s
add wave -noupdate /tb4counter/cnt_argl_s
add wave -noupdate -divider -height 50 aril
add wave -noupdate /tb4counter/did_aril_s
add wave -noupdate /tb4counter/err_aril_s
add wave -noupdate /tb4counter/cnt_aril_s
add wave -noupdate -divider -height 50 ok
add wave -noupdate /tb4counter/ok_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {225170 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 210
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1708490 ps}
