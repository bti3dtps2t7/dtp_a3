
library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic

entity tb4counter is
end entity tb4counter;


architecture beh of tb4counter is
	signal ValLd_s : std_logic_vector(11 downto 0) := (others=>'0');
	signal up_s : std_logic  := '0';
	signal down_s : std_logic := '0';
	signal nLd_s : std_logic := '1';
	signal clk_s : std_logic;
	signal nres_s : std_logic;
	signal did_arop_s : std_logic_vector( 2 downto 0 ) := (others=>'0');
    signal err_arop_s : std_logic := '0';
   	signal cnt_arop_s : std_logic_vector( 11 downto 0 ) := (others=>'0');
   	signal did_argl_s : std_logic_vector( 2 downto 0 ) := (others=>'0');
    signal err_argl_s : std_logic := '0';
   	signal cnt_argl_s : std_logic_vector( 11 downto 0 ) := (others=>'0');
   	signal did_aril_s : std_logic_vector( 2 downto 0 ) := (others=>'0');
    signal err_aril_s : std_logic := '0';
   	signal cnt_aril_s : std_logic_vector( 11 downto 0 ) := (others=>'0');
   	signal ok_s : std_logic;


    component sg4counter is
        port(
        	sg_ValLd : out std_logic_vector(11 downto 0);
        	sg_up : out std_logic;
        	sg_down : out std_logic;
        	sg_nLd : out std_logic;
        	sg_clk : out std_logic;
        	sg_nres : out std_logic
        );--]port
    end component sg4counter;
    for all : sg4counter use entity work.sg4counter( beh );

    component rc4counter is
    	port(
            clk : in std_logic;
			err_arop : in std_logic;
			cnt_arop : in std_logic_vector(11 downto 0);
			err_argl : in std_logic;
			cnt_argl : in std_logic_vector(11 downto 0);
			err_aril : in std_logic;
			cnt_aril : in std_logic_vector(11 downto 0);
			ok : out std_logic
    		);
    end component rc4counter;
    for all : rc4counter use entity work.rc4counter( beh );


    component counter is
    port ( 
    	did   : out std_logic_vector(  2 downto 0 );  --Dut IDentifier
    	err   : out std_logic;                        --ERRor : invalid counter value
    	cnt   : out std_logic_vector( 11 downto 0 );  --CouNT
    	valLd : in  std_logic_vector( 11 downto 0 );  --init VALue in case of LoaD
    	nLd   : in  std_logic;                        --Not LoaD; low actve LoaD
    	up    : in  std_logic;                        --UP count command
    	down  : in  std_logic;                        --DOWN count command
    	clk   : in  std_logic;                        --CLocK
    	nres  : in  std_logic                         --Not RESet ; low active synchronous reset
  	);--]port
  	end component counter;
  	for cnt_1 : counter use entity work.counter( arop );
  	for cnt_2 : counter use entity work.counter( argl );
  	for cnt_3 : counter use entity work.counter( aril );
begin



sg_i : sg4counter
        port map (
           	sg_ValLd => ValLd_s,
        	sg_up => up_s,
        	sg_down => down_s,
        	sg_nLd => nLd_s,
        	sg_clk => clk_s,
        	sg_nres => nres_s
        )--]port
    ;--]sg_i


cnt_1 : counter
    port map (
        valLd => ValLd_s,
        nLd => nLd_s,
        up => up_s,
        down => down_s,
        clk => clk_s,
        nres => nres_s,
        did => did_arop_s,
        err => err_arop_s,
        cnt => cnt_arop_s
    )--]port
;--]ha_i1

cnt_2 : counter
    port map (
        valLd => ValLd_s,
        nLd => nLd_s,
        up => up_s,
        down => down_s,
        clk => clk_s,
        nres => nres_s,
        did => did_argl_s,
        err => err_argl_s,
        cnt => cnt_argl_s
    )--]port
;--]ha_i1

cnt_3 : counter
    port map (
        valLd => ValLd_s,
        nLd => nLd_s,
        up => up_s,
        down => down_s,
        clk => clk_s,
        nres => nres_s,
        did => did_aril_s,
        err => err_aril_s,
        cnt => cnt_aril_s
    )--]port
;--]ha_i1


rc_i : rc4counter
	port map(
        clk => clk_s,
		err_arop => err_arop_s,
		cnt_arop => cnt_arop_s,
		err_argl => err_argl_s,
		cnt_argl => cnt_argl_s,
		err_aril => err_aril_s,
		cnt_aril =>	cnt_aril_s,
		ok => ok_s
	)
	;




end architecture beh;