--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: DUT_timesim.vhd
-- /___/   /\     Timestamp: Fri Dec 11 13:41:53 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm DUT -w -dir netgen/fit -ofmt vhdl -sim DUT.nga DUT_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: DUT.nga
-- Output file	: D:\DTP\dtp_a3\dt_aufgabe3\ise_14x4\iseWRK\netgen\fit\DUT_timesim.vhd
-- # of Entities	: 1
-- Design Name	: DUT.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;


architecture Structure_atil of DUT is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal down_II_UIM_5 : STD_LOGIC; 
  signal up_II_UIM_7 : STD_LOGIC; 
  signal nLd_II_UIM_9 : STD_LOGIC; 
  signal valLd_0_II_UIM_11 : STD_LOGIC; 
  signal valLd_1_II_UIM_13 : STD_LOGIC; 
  signal valLd_3_II_UIM_15 : STD_LOGIC; 
  signal valLd_2_II_UIM_17 : STD_LOGIC; 
  signal valLd_5_II_UIM_19 : STD_LOGIC; 
  signal valLd_4_II_UIM_21 : STD_LOGIC; 
  signal valLd_7_II_UIM_23 : STD_LOGIC; 
  signal valLd_6_II_UIM_25 : STD_LOGIC; 
  signal valLd_9_II_UIM_27 : STD_LOGIC; 
  signal valLd_8_II_UIM_29 : STD_LOGIC; 
  signal valLd_10_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal cnt_0_MC_Q_35 : STD_LOGIC; 
  signal cnt_10_MC_Q_37 : STD_LOGIC; 
  signal cnt_11_MC_Q_39 : STD_LOGIC; 
  signal cnt_1_MC_Q_41 : STD_LOGIC; 
  signal cnt_2_MC_Q_43 : STD_LOGIC; 
  signal cnt_3_MC_Q_45 : STD_LOGIC; 
  signal cnt_4_MC_Q_47 : STD_LOGIC; 
  signal cnt_5_MC_Q_49 : STD_LOGIC; 
  signal cnt_6_MC_Q_51 : STD_LOGIC; 
  signal cnt_7_MC_Q_53 : STD_LOGIC; 
  signal cnt_8_MC_Q_55 : STD_LOGIC; 
  signal cnt_9_MC_Q_57 : STD_LOGIC; 
  signal did_0_MC_Q_59 : STD_LOGIC; 
  signal did_1_MC_Q_61 : STD_LOGIC; 
  signal did_2_MC_Q_63 : STD_LOGIC; 
  signal err_MC_Q_65 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_D_67 : STD_LOGIC; 
  signal Gnd_68 : STD_LOGIC; 
  signal Vcc_69 : STD_LOGIC; 
  signal cnt_0_MC_D1_70 : STD_LOGIC; 
  signal cnt_0_MC_D2_71 : STD_LOGIC; 
  signal cnt_1_down_cs_72 : STD_LOGIC; 
  signal cnt_1_up_cs_73 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_75 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_76 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_Q : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D_78 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D1_79 : STD_LOGIC; 
  signal cnt_1_down_cs_MC_D2_80 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_Q : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D_82 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D1_83 : STD_LOGIC; 
  signal cnt_1_up_cs_MC_D2_84 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D_86 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D1_87 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_88 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_0_89 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_1_90 : STD_LOGIC; 
  signal cnt_1_valLd_cs_0_MC_D2_PT_2_91 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_10_MC_D_93 : STD_LOGIC; 
  signal cnt_10_MC_D1_94 : STD_LOGIC; 
  signal cnt_10_MC_D2_95 : STD_LOGIC; 
  signal N_PZ_165_97 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_0_98 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_1_101 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_2_109 : STD_LOGIC; 
  signal cnt_10_MC_D2_PT_3_110 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D_112 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_113 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D1_114 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_115 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_0_116 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_1_117 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_2_118 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_3_119 : STD_LOGIC; 
  signal cnt_1_valLd_cs_1_MC_D2_PT_4_120 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D_122 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_123 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D1_124 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_125 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_0_126 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_1_127 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_2_128 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_3_129 : STD_LOGIC; 
  signal cnt_1_valLd_cs_3_MC_D2_PT_4_130 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D_132 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_133 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D1_134 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_135 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_0_136 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_1_137 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_2_138 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_3_139 : STD_LOGIC; 
  signal cnt_1_valLd_cs_2_MC_D2_PT_4_140 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D_142 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_143 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D1_144 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_145 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_0_146 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_1_147 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_2_148 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_3_149 : STD_LOGIC; 
  signal cnt_1_valLd_cs_5_MC_D2_PT_4_150 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D_152 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_153 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D1_154 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_155 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_0_156 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_1_157 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_2_158 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_3_159 : STD_LOGIC; 
  signal cnt_1_valLd_cs_4_MC_D2_PT_4_160 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D_162 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_163 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D1_164 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_165 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_0_166 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_1_167 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_2_168 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_3_169 : STD_LOGIC; 
  signal cnt_1_valLd_cs_7_MC_D2_PT_4_170 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D_172 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_173 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D1_174 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_175 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_0_176 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_1_177 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_2_178 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_3_179 : STD_LOGIC; 
  signal cnt_1_valLd_cs_6_MC_D2_PT_4_180 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D_182 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q_183 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D1_184 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_185 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_0_186 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_1_187 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_2_188 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_3_189 : STD_LOGIC; 
  signal cnt_1_valLd_cs_9_MC_D2_PT_4_190 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D_192 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q_193 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D1_194 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_195 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_0_196 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_1_197 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_2_198 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_3_199 : STD_LOGIC; 
  signal cnt_1_valLd_cs_8_MC_D2_PT_4_200 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D_202 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q_203 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D1_204 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_205 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_0_206 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_1_207 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_2_208 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_3_209 : STD_LOGIC; 
  signal cnt_1_valLd_cs_10_MC_D2_PT_4_210 : STD_LOGIC; 
  signal N_PZ_165_MC_Q_211 : STD_LOGIC; 
  signal N_PZ_165_MC_D_212 : STD_LOGIC; 
  signal N_PZ_165_MC_D1_213 : STD_LOGIC; 
  signal N_PZ_165_MC_D2_214 : STD_LOGIC; 
  signal N_PZ_165_MC_D2_PT_0_215 : STD_LOGIC; 
  signal N_PZ_165_MC_D2_PT_1_216 : STD_LOGIC; 
  signal N_PZ_165_MC_D2_PT_2_217 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_11_MC_D_219 : STD_LOGIC; 
  signal cnt_11_MC_D1_220 : STD_LOGIC; 
  signal cnt_11_MC_D2_221 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_0_223 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_1_224 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_2_225 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_3_226 : STD_LOGIC; 
  signal cnt_11_MC_D2_PT_4_227 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_Q : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D_229 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q_230 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D1_231 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_232 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_0_233 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_1_234 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_2_235 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_3_236 : STD_LOGIC; 
  signal cnt_1_valLd_cs_11_MC_D2_PT_4_237 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_239 : STD_LOGIC; 
  signal cnt_1_MC_D1_240 : STD_LOGIC; 
  signal cnt_1_MC_D2_241 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_242 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_243 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_245 : STD_LOGIC; 
  signal cnt_2_MC_D1_246 : STD_LOGIC; 
  signal cnt_2_MC_D2_247 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_248 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_249 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_250 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_3_MC_D_252 : STD_LOGIC; 
  signal cnt_3_MC_D1_253 : STD_LOGIC; 
  signal cnt_3_MC_D2_254 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_0_255 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_1_256 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_2_257 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_4_MC_D_259 : STD_LOGIC; 
  signal cnt_4_MC_D1_260 : STD_LOGIC; 
  signal cnt_4_MC_D2_261 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_0_262 : STD_LOGIC; 
  signal cnt_4_MC_D2_PT_1_263 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_5_MC_D_265 : STD_LOGIC; 
  signal cnt_5_MC_D1_266 : STD_LOGIC; 
  signal cnt_5_MC_D2_267 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_0_268 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_1_269 : STD_LOGIC; 
  signal cnt_5_MC_D2_PT_2_270 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_6_MC_D_272 : STD_LOGIC; 
  signal cnt_6_MC_D1_273 : STD_LOGIC; 
  signal cnt_6_MC_D2_274 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_0_275 : STD_LOGIC; 
  signal cnt_6_MC_D2_PT_1_276 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_7_MC_D_278 : STD_LOGIC; 
  signal cnt_7_MC_D1_279 : STD_LOGIC; 
  signal cnt_7_MC_D2_280 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_0_281 : STD_LOGIC; 
  signal cnt_7_MC_D2_PT_1_282 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_8_MC_D_284 : STD_LOGIC; 
  signal cnt_8_MC_D1_285 : STD_LOGIC; 
  signal cnt_8_MC_D2_286 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_0_287 : STD_LOGIC; 
  signal cnt_8_MC_D2_PT_1_288 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_9_MC_D_290 : STD_LOGIC; 
  signal cnt_9_MC_D1_291 : STD_LOGIC; 
  signal cnt_9_MC_D2_292 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_0_293 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_1_294 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_2_295 : STD_LOGIC; 
  signal cnt_9_MC_D2_PT_3_296 : STD_LOGIC; 
  signal did_0_MC_Q_tsimrenamed_net_Q_297 : STD_LOGIC; 
  signal did_0_MC_D_298 : STD_LOGIC; 
  signal did_0_MC_D1_299 : STD_LOGIC; 
  signal did_0_MC_D2_300 : STD_LOGIC; 
  signal did_1_MC_Q_tsimrenamed_net_Q_301 : STD_LOGIC; 
  signal did_1_MC_D_302 : STD_LOGIC; 
  signal did_1_MC_D1_303 : STD_LOGIC; 
  signal did_1_MC_D2_304 : STD_LOGIC; 
  signal did_2_MC_Q_tsimrenamed_net_Q_305 : STD_LOGIC; 
  signal did_2_MC_D_306 : STD_LOGIC; 
  signal did_2_MC_D1_307 : STD_LOGIC; 
  signal did_2_MC_D2_308 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_UIM_310 : STD_LOGIC; 
  signal err_MC_D_311 : STD_LOGIC; 
  signal err_MC_D1_312 : STD_LOGIC; 
  signal err_MC_D2_313 : STD_LOGIC; 
  signal err_MC_D2_PT_0_314 : STD_LOGIC; 
  signal err_MC_D2_PT_1_315 : STD_LOGIC; 
  signal err_MC_D2_PT_2_316 : STD_LOGIC; 
  signal err_MC_D2_PT_3_317 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_165_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_0_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_0_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_0_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_PT_1_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_did_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_165_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_1_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_0_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_did_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal cnt_1_valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_5
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_7
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_9
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_11
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_13
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_15
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_17
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_19
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_21
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_23
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_25
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_27
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_29
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_35,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_37,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_39,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_41,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_43,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_45,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_47,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_49,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_51,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_53,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_55,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_57,
      O => cnt(9)
    );
  did_0_Q : X_BUF
    port map (
      I => did_0_MC_Q_59,
      O => did(0)
    );
  did_1_Q : X_BUF
    port map (
      I => did_1_MC_Q_61,
      O => did(1)
    );
  did_2_Q : X_BUF
    port map (
      I => did_2_MC_Q_63,
      O => did(2)
    );
  err_66 : X_BUF
    port map (
      I => err_MC_Q_65,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_35
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_68
    );
  Vcc : X_ONE
    port map (
      O => Vcc_69
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_67
    );
  cnt_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D1_IN1,
      O => cnt_0_MC_D1_70
    );
  cnt_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN2,
      O => cnt_0_MC_D2_PT_0_75
    );
  cnt_0_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2,
      O => cnt_0_MC_D2_PT_1_76
    );
  cnt_0_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      O => cnt_0_MC_D2_71
    );
  cnt_1_down_cs : X_BUF
    port map (
      I => cnt_1_down_cs_MC_Q,
      O => cnt_1_down_cs_72
    );
  cnt_1_down_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_down_cs_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_down_cs_MC_Q
    );
  cnt_1_down_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_down_cs_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_down_cs_MC_D_IN1,
      O => cnt_1_down_cs_MC_D_78
    );
  cnt_1_down_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1,
      O => cnt_1_down_cs_MC_D1_79
    );
  cnt_1_down_cs_MC_D2 : X_ZERO
    port map (
      O => cnt_1_down_cs_MC_D2_80
    );
  cnt_1_up_cs : X_BUF
    port map (
      I => cnt_1_up_cs_MC_Q,
      O => cnt_1_up_cs_73
    );
  cnt_1_up_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_up_cs_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_up_cs_MC_Q
    );
  cnt_1_up_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_up_cs_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_up_cs_MC_D_IN1,
      O => cnt_1_up_cs_MC_D_82
    );
  cnt_1_up_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1,
      O => cnt_1_up_cs_MC_D1_83
    );
  cnt_1_up_cs_MC_D2 : X_ZERO
    port map (
      O => cnt_1_up_cs_MC_D2_84
    );
  cnt_1_valLd_cs_0_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_Q,
      O => cnt_1_valLd_cs(0)
    );
  cnt_1_valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_0_MC_Q
    );
  cnt_1_valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1,
      O => cnt_1_valLd_cs_0_MC_D_86
    );
  cnt_1_valLd_cs_0_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN2,
      O => cnt_1_valLd_cs_0_MC_D1_87
    );
  cnt_1_valLd_cs_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2,
      O => cnt_1_valLd_cs_0_MC_D2_PT_0_89
    );
  cnt_1_valLd_cs_0_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3,
      O => cnt_1_valLd_cs_0_MC_D2_PT_1_90
    );
  cnt_1_valLd_cs_0_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_0_MC_D2_PT_2_91
    );
  cnt_1_valLd_cs_0_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN2,
      O => cnt_1_valLd_cs_0_MC_D2_88
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q,
      O => cnt_10_MC_Q_37
    );
  cnt_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_10_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_10_MC_Q_tsimrenamed_net_Q
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_93
    );
  cnt_10_MC_D1 : X_ZERO
    port map (
      O => cnt_10_MC_D1_94
    );
  cnt_10_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1,
      O => cnt_10_MC_D2_PT_0_98
    );
  cnt_10_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN3,
      O => cnt_10_MC_D2_PT_1_101
    );
  cnt_10_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN8,
      I9 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN9,
      I10 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN10,
      I11 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN11,
      I12 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN12,
      I13 => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN15,
      O => cnt_10_MC_D2_PT_2_109
    );
  cnt_10_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN15,
      O => cnt_10_MC_D2_PT_3_110
    );
  cnt_10_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_10_MC_D2_IN3,
      O => cnt_10_MC_D2_95
    );
  cnt_1_valLd_cs_1_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_Q,
      O => cnt_1_valLd_cs(1)
    );
  cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_113
    );
  cnt_1_valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_1_MC_Q
    );
  cnt_1_valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1,
      O => cnt_1_valLd_cs_1_MC_D_112
    );
  cnt_1_valLd_cs_1_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_1_MC_D1_114
    );
  cnt_1_valLd_cs_1_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_1_MC_D2_PT_0_116
    );
  cnt_1_valLd_cs_1_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_1_MC_D2_PT_1_117
    );
  cnt_1_valLd_cs_1_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_1_MC_D2_PT_2_118
    );
  cnt_1_valLd_cs_1_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_PT_3_119
    );
  cnt_1_valLd_cs_1_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_PT_4_120
    );
  cnt_1_valLd_cs_1_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4,
      O => cnt_1_valLd_cs_1_MC_D2_115
    );
  cnt_1_valLd_cs_3_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_Q,
      O => cnt_1_valLd_cs(3)
    );
  cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_123
    );
  cnt_1_valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_3_MC_Q
    );
  cnt_1_valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1,
      O => cnt_1_valLd_cs_3_MC_D_122
    );
  cnt_1_valLd_cs_3_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_3_MC_D1_124
    );
  cnt_1_valLd_cs_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_3_MC_D2_PT_0_126
    );
  cnt_1_valLd_cs_3_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_3_MC_D2_PT_1_127
    );
  cnt_1_valLd_cs_3_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_3_MC_D2_PT_2_128
    );
  cnt_1_valLd_cs_3_MC_D2_PT_3 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6,
      O => cnt_1_valLd_cs_3_MC_D2_PT_3_129
    );
  cnt_1_valLd_cs_3_MC_D2_PT_4 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6,
      O => cnt_1_valLd_cs_3_MC_D2_PT_4_130
    );
  cnt_1_valLd_cs_3_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4,
      O => cnt_1_valLd_cs_3_MC_D2_125
    );
  cnt_1_valLd_cs_2_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_Q,
      O => cnt_1_valLd_cs(2)
    );
  cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_133
    );
  cnt_1_valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_2_MC_Q
    );
  cnt_1_valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1,
      O => cnt_1_valLd_cs_2_MC_D_132
    );
  cnt_1_valLd_cs_2_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_2_MC_D1_134
    );
  cnt_1_valLd_cs_2_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_2_MC_D2_PT_0_136
    );
  cnt_1_valLd_cs_2_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_2_MC_D2_PT_1_137
    );
  cnt_1_valLd_cs_2_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_2_MC_D2_PT_2_138
    );
  cnt_1_valLd_cs_2_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5,
      O => cnt_1_valLd_cs_2_MC_D2_PT_3_139
    );
  cnt_1_valLd_cs_2_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5,
      O => cnt_1_valLd_cs_2_MC_D2_PT_4_140
    );
  cnt_1_valLd_cs_2_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4,
      O => cnt_1_valLd_cs_2_MC_D2_135
    );
  cnt_1_valLd_cs_5_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_Q,
      O => cnt_1_valLd_cs(5)
    );
  cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_143
    );
  cnt_1_valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_5_MC_Q
    );
  cnt_1_valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1,
      O => cnt_1_valLd_cs_5_MC_D_142
    );
  cnt_1_valLd_cs_5_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_5_MC_D1_144
    );
  cnt_1_valLd_cs_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_5_MC_D2_PT_0_146
    );
  cnt_1_valLd_cs_5_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_5_MC_D2_PT_1_147
    );
  cnt_1_valLd_cs_5_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_5_MC_D2_PT_2_148
    );
  cnt_1_valLd_cs_5_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_5_MC_D2_PT_3_149
    );
  cnt_1_valLd_cs_5_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_5_MC_D2_PT_4_150
    );
  cnt_1_valLd_cs_5_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4,
      O => cnt_1_valLd_cs_5_MC_D2_145
    );
  cnt_1_valLd_cs_4_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_Q,
      O => cnt_1_valLd_cs(4)
    );
  cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_153
    );
  cnt_1_valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_4_MC_Q
    );
  cnt_1_valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1,
      O => cnt_1_valLd_cs_4_MC_D_152
    );
  cnt_1_valLd_cs_4_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_4_MC_D1_154
    );
  cnt_1_valLd_cs_4_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_4_MC_D2_PT_0_156
    );
  cnt_1_valLd_cs_4_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_4_MC_D2_PT_1_157
    );
  cnt_1_valLd_cs_4_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_4_MC_D2_PT_2_158
    );
  cnt_1_valLd_cs_4_MC_D2_PT_3 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7,
      O => cnt_1_valLd_cs_4_MC_D2_PT_3_159
    );
  cnt_1_valLd_cs_4_MC_D2_PT_4 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7,
      O => cnt_1_valLd_cs_4_MC_D2_PT_4_160
    );
  cnt_1_valLd_cs_4_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4,
      O => cnt_1_valLd_cs_4_MC_D2_155
    );
  cnt_1_valLd_cs_7_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_Q,
      O => cnt_1_valLd_cs(7)
    );
  cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_163
    );
  cnt_1_valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_7_MC_Q
    );
  cnt_1_valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1,
      O => cnt_1_valLd_cs_7_MC_D_162
    );
  cnt_1_valLd_cs_7_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_7_MC_D1_164
    );
  cnt_1_valLd_cs_7_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_7_MC_D2_PT_0_166
    );
  cnt_1_valLd_cs_7_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_7_MC_D2_PT_1_167
    );
  cnt_1_valLd_cs_7_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_7_MC_D2_PT_2_168
    );
  cnt_1_valLd_cs_7_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_7_MC_D2_PT_3_169
    );
  cnt_1_valLd_cs_7_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_7_MC_D2_PT_4_170
    );
  cnt_1_valLd_cs_7_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4,
      O => cnt_1_valLd_cs_7_MC_D2_165
    );
  cnt_1_valLd_cs_6_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_Q,
      O => cnt_1_valLd_cs(6)
    );
  cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_173
    );
  cnt_1_valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_6_MC_Q
    );
  cnt_1_valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1,
      O => cnt_1_valLd_cs_6_MC_D_172
    );
  cnt_1_valLd_cs_6_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_6_MC_D1_174
    );
  cnt_1_valLd_cs_6_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_6_MC_D2_PT_0_176
    );
  cnt_1_valLd_cs_6_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_6_MC_D2_PT_1_177
    );
  cnt_1_valLd_cs_6_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_6_MC_D2_PT_2_178
    );
  cnt_1_valLd_cs_6_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_6_MC_D2_PT_3_179
    );
  cnt_1_valLd_cs_6_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_6_MC_D2_PT_4_180
    );
  cnt_1_valLd_cs_6_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4,
      O => cnt_1_valLd_cs_6_MC_D2_175
    );
  cnt_1_valLd_cs_9_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_Q,
      O => cnt_1_valLd_cs(9)
    );
  cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q_183
    );
  cnt_1_valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_9_MC_Q
    );
  cnt_1_valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1,
      O => cnt_1_valLd_cs_9_MC_D_182
    );
  cnt_1_valLd_cs_9_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_9_MC_D1_184
    );
  cnt_1_valLd_cs_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_9_MC_D2_PT_0_186
    );
  cnt_1_valLd_cs_9_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_9_MC_D2_PT_1_187
    );
  cnt_1_valLd_cs_9_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_9_MC_D2_PT_2_188
    );
  cnt_1_valLd_cs_9_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10,
      I11 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11,
      I12 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_9_MC_D2_PT_3_189
    );
  cnt_1_valLd_cs_9_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_9_MC_D2_PT_4_190
    );
  cnt_1_valLd_cs_9_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN4,
      O => cnt_1_valLd_cs_9_MC_D2_185
    );
  cnt_1_valLd_cs_8_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_Q,
      O => cnt_1_valLd_cs(8)
    );
  cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q_193
    );
  cnt_1_valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_8_MC_Q
    );
  cnt_1_valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1,
      O => cnt_1_valLd_cs_8_MC_D_192
    );
  cnt_1_valLd_cs_8_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_8_MC_D1_194
    );
  cnt_1_valLd_cs_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_8_MC_D2_PT_0_196
    );
  cnt_1_valLd_cs_8_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_8_MC_D2_PT_1_197
    );
  cnt_1_valLd_cs_8_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_8_MC_D2_PT_2_198
    );
  cnt_1_valLd_cs_8_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10,
      I11 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_8_MC_D2_PT_3_199
    );
  cnt_1_valLd_cs_8_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_8_MC_D2_PT_4_200
    );
  cnt_1_valLd_cs_8_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4,
      O => cnt_1_valLd_cs_8_MC_D2_195
    );
  cnt_1_valLd_cs_10_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_Q,
      O => cnt_1_valLd_cs(10)
    );
  cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q_203
    );
  cnt_1_valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_10_MC_Q
    );
  cnt_1_valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1,
      O => cnt_1_valLd_cs_10_MC_D_202
    );
  cnt_1_valLd_cs_10_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_10_MC_D1_204
    );
  cnt_1_valLd_cs_10_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_10_MC_D2_PT_0_206
    );
  cnt_1_valLd_cs_10_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_10_MC_D2_PT_1_207
    );
  cnt_1_valLd_cs_10_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_10_MC_D2_PT_2_208
    );
  cnt_1_valLd_cs_10_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10,
      I11 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11,
      I12 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12,
      I13 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_10_MC_D2_PT_3_209
    );
  cnt_1_valLd_cs_10_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_10_MC_D2_PT_4_210
    );
  cnt_1_valLd_cs_10_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN4,
      O => cnt_1_valLd_cs_10_MC_D2_205
    );
  N_PZ_165 : X_BUF
    port map (
      I => N_PZ_165_MC_Q_211,
      O => N_PZ_165_97
    );
  N_PZ_165_MC_Q : X_BUF
    port map (
      I => N_PZ_165_MC_D_212,
      O => N_PZ_165_MC_Q_211
    );
  N_PZ_165_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_165_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_165_MC_D_IN1,
      O => N_PZ_165_MC_D_212
    );
  N_PZ_165_MC_D1 : X_ZERO
    port map (
      O => N_PZ_165_MC_D1_213
    );
  N_PZ_165_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_165_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_165_MC_D2_PT_0_IN1,
      O => N_PZ_165_MC_D2_PT_0_215
    );
  N_PZ_165_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN6,
      I7 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN7,
      I8 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN8,
      I9 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN9,
      I10 => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN15,
      O => N_PZ_165_MC_D2_PT_1_216
    );
  N_PZ_165_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwInverterSignal_N_PZ_165_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN7,
      I8 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN15,
      O => N_PZ_165_MC_D2_PT_2_217
    );
  N_PZ_165_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_N_PZ_165_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_165_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_165_MC_D2_IN2,
      O => N_PZ_165_MC_D2_214
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q,
      O => cnt_11_MC_Q_39
    );
  cnt_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_11_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_11_MC_Q_tsimrenamed_net_Q
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_219
    );
  cnt_11_MC_D1 : X_ZERO
    port map (
      O => cnt_11_MC_D1_220
    );
  cnt_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1,
      O => cnt_11_MC_D2_PT_0_223
    );
  cnt_11_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3,
      O => cnt_11_MC_D2_PT_1_224
    );
  cnt_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3,
      O => cnt_11_MC_D2_PT_2_225
    );
  cnt_11_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN10,
      I11 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN11,
      I12 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN12,
      I13 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN13,
      I14 => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN15,
      O => cnt_11_MC_D2_PT_3_226
    );
  cnt_11_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN13,
      I14 => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN15,
      O => cnt_11_MC_D2_PT_4_227
    );
  cnt_11_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_11_MC_D2_IN4,
      O => cnt_11_MC_D2_221
    );
  cnt_1_valLd_cs_11_Q : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_Q,
      O => cnt_1_valLd_cs(11)
    );
  cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1,
      O => cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q_230
    );
  cnt_1_valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_valLd_cs_11_MC_Q
    );
  cnt_1_valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1,
      O => cnt_1_valLd_cs_11_MC_D_229
    );
  cnt_1_valLd_cs_11_MC_D1 : X_ZERO
    port map (
      O => cnt_1_valLd_cs_11_MC_D1_231
    );
  cnt_1_valLd_cs_11_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1,
      O => cnt_1_valLd_cs_11_MC_D2_PT_0_233
    );
  cnt_1_valLd_cs_11_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2,
      O => cnt_1_valLd_cs_11_MC_D2_PT_1_234
    );
  cnt_1_valLd_cs_11_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3,
      O => cnt_1_valLd_cs_11_MC_D2_PT_2_235
    );
  cnt_1_valLd_cs_11_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5,
      I6 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8,
      I9 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9,
      I10 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10,
      I11 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11,
      I12 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12,
      I13 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13,
      I14 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15,
      O => cnt_1_valLd_cs_11_MC_D2_PT_3_236
    );
  cnt_1_valLd_cs_11_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4,
      I5 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15,
      O => cnt_1_valLd_cs_11_MC_D2_PT_4_237
    );
  cnt_1_valLd_cs_11_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN4,
      O => cnt_1_valLd_cs_11_MC_D2_232
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_41
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_239
    );
  cnt_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D1_IN1,
      O => cnt_1_MC_D1_240
    );
  cnt_1_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3,
      O => cnt_1_MC_D2_PT_0_242
    );
  cnt_1_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3,
      O => cnt_1_MC_D2_PT_1_243
    );
  cnt_1_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      O => cnt_1_MC_D2_241
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_43
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_245
    );
  cnt_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D1_IN1,
      O => cnt_2_MC_D1_246
    );
  cnt_2_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN4,
      O => cnt_2_MC_D2_PT_0_248
    );
  cnt_2_MC_D2_PT_1 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN5,
      O => cnt_2_MC_D2_PT_1_249
    );
  cnt_2_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN5,
      O => cnt_2_MC_D2_PT_2_250
    );
  cnt_2_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      O => cnt_2_MC_D2_247
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_Q_45
    );
  cnt_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_3_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_3_MC_Q_tsimrenamed_net_Q
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_252
    );
  cnt_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D1_IN1,
      O => cnt_3_MC_D1_253
    );
  cnt_3_MC_D2_PT_0 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN5,
      O => cnt_3_MC_D2_PT_0_255
    );
  cnt_3_MC_D2_PT_1 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN6,
      O => cnt_3_MC_D2_PT_1_256
    );
  cnt_3_MC_D2_PT_2 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN6,
      O => cnt_3_MC_D2_PT_2_257
    );
  cnt_3_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_IN2,
      O => cnt_3_MC_D2_254
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q,
      O => cnt_4_MC_Q_47
    );
  cnt_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_4_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_4_MC_Q_tsimrenamed_net_Q
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_259
    );
  cnt_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D1_IN1,
      O => cnt_4_MC_D1_260
    );
  cnt_4_MC_D2_PT_0 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN6,
      O => cnt_4_MC_D2_PT_0_262
    );
  cnt_4_MC_D2_PT_1 : X_AND7
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN6,
      O => cnt_4_MC_D2_PT_1_263
    );
  cnt_4_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D2_IN1,
      O => cnt_4_MC_D2_261
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q,
      O => cnt_5_MC_Q_49
    );
  cnt_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_5_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_5_MC_Q_tsimrenamed_net_Q
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_265
    );
  cnt_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D1_IN1,
      O => cnt_5_MC_D1_266
    );
  cnt_5_MC_D2_PT_0 : X_AND8
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN4,
      I5 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN5,
      I6 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN7,
      O => cnt_5_MC_D2_PT_0_268
    );
  cnt_5_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN5,
      I6 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN7,
      I8 => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN15,
      O => cnt_5_MC_D2_PT_1_269
    );
  cnt_5_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN15,
      O => cnt_5_MC_D2_PT_2_270
    );
  cnt_5_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_5_MC_D2_IN2,
      O => cnt_5_MC_D2_267
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q,
      O => cnt_6_MC_Q_51
    );
  cnt_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_6_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_6_MC_Q_tsimrenamed_net_Q
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_272
    );
  cnt_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D1_IN1,
      O => cnt_6_MC_D1_273
    );
  cnt_6_MC_D2_PT_0 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN6,
      I7 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN7,
      I8 => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN15,
      O => cnt_6_MC_D2_PT_0_275
    );
  cnt_6_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN7,
      I8 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN15,
      O => cnt_6_MC_D2_PT_1_276
    );
  cnt_6_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D2_IN1,
      O => cnt_6_MC_D2_274
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q,
      O => cnt_7_MC_Q_53
    );
  cnt_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_7_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_7_MC_Q_tsimrenamed_net_Q
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_278
    );
  cnt_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D1_IN1,
      O => cnt_7_MC_D1_279
    );
  cnt_7_MC_D2_PT_0 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN6,
      I7 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN7,
      I8 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN8,
      I9 => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN9,
      I10 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN10,
      I11 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN11,
      I12 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN12,
      I13 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN13,
      I14 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN14,
      I15 => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN15,
      O => cnt_7_MC_D2_PT_0_281
    );
  cnt_7_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN7,
      I8 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN15,
      O => cnt_7_MC_D2_PT_1_282
    );
  cnt_7_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D2_IN1,
      O => cnt_7_MC_D2_280
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q,
      O => cnt_8_MC_Q_55
    );
  cnt_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_8_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_8_MC_Q_tsimrenamed_net_Q
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_284
    );
  cnt_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D1_IN1,
      O => cnt_8_MC_D1_285
    );
  cnt_8_MC_D2_PT_0 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN4,
      I5 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN5,
      I6 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN6,
      I7 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN7,
      I8 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN8,
      I9 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN9,
      I10 => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN10,
      I11 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN15,
      O => cnt_8_MC_D2_PT_0_287
    );
  cnt_8_MC_D2_PT_1 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN6,
      I7 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN7,
      I8 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN8,
      I9 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN9,
      I10 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN10,
      I11 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN11,
      I12 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN12,
      I13 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN13,
      I14 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN14,
      I15 => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN15,
      O => cnt_8_MC_D2_PT_1_288
    );
  cnt_8_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D2_IN1,
      O => cnt_8_MC_D2_286
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q,
      O => cnt_9_MC_Q_57
    );
  cnt_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_cnt_9_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => cnt_9_MC_Q_tsimrenamed_net_Q
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_290
    );
  cnt_9_MC_D1 : X_ZERO
    port map (
      O => cnt_9_MC_D1_291
    );
  cnt_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1,
      O => cnt_9_MC_D2_PT_0_293
    );
  cnt_9_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3,
      O => cnt_9_MC_D2_PT_1_294
    );
  cnt_9_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN8,
      I9 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN9,
      I10 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN10,
      I11 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN11,
      I12 => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN15,
      O => cnt_9_MC_D2_PT_2_295
    );
  cnt_9_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15,
      O => cnt_9_MC_D2_PT_3_296
    );
  cnt_9_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_9_MC_D2_IN3,
      O => cnt_9_MC_D2_292
    );
  did_0_MC_Q : X_BUF
    port map (
      I => did_0_MC_Q_tsimrenamed_net_Q_297,
      O => did_0_MC_Q_59
    );
  did_0_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_0_MC_D_298,
      O => did_0_MC_Q_tsimrenamed_net_Q_297
    );
  did_0_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_did_0_MC_D_IN0,
      I1 => NlwBufferSignal_did_0_MC_D_IN1,
      O => did_0_MC_D_298
    );
  did_0_MC_D1 : X_ZERO
    port map (
      O => did_0_MC_D1_299
    );
  did_0_MC_D2 : X_ZERO
    port map (
      O => did_0_MC_D2_300
    );
  did_1_MC_Q : X_BUF
    port map (
      I => did_1_MC_Q_tsimrenamed_net_Q_301,
      O => did_1_MC_Q_61
    );
  did_1_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_1_MC_D_302,
      O => did_1_MC_Q_tsimrenamed_net_Q_301
    );
  did_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_1_MC_D_IN0,
      I1 => NlwBufferSignal_did_1_MC_D_IN1,
      O => did_1_MC_D_302
    );
  did_1_MC_D1 : X_ZERO
    port map (
      O => did_1_MC_D1_303
    );
  did_1_MC_D2 : X_ZERO
    port map (
      O => did_1_MC_D2_304
    );
  did_2_MC_Q : X_BUF
    port map (
      I => did_2_MC_Q_tsimrenamed_net_Q_305,
      O => did_2_MC_Q_63
    );
  did_2_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => did_2_MC_D_306,
      O => did_2_MC_Q_tsimrenamed_net_Q_305
    );
  did_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_did_2_MC_D_IN0,
      I1 => NlwBufferSignal_did_2_MC_D_IN1,
      O => did_2_MC_D_306
    );
  did_2_MC_D1 : X_ZERO
    port map (
      O => did_2_MC_D1_307
    );
  did_2_MC_D2 : X_ZERO
    port map (
      O => did_2_MC_D2_308
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_65
    );
  err_MC_UIM : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_UIM_310
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_69,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_68,
      RST => Gnd_68,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_311
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_312
    );
  err_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      O => err_MC_D2_PT_0_314
    );
  err_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_1_IN2,
      O => err_MC_D2_PT_1_315
    );
  err_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_err_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_err_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_err_MC_D2_PT_2_IN8,
      I9 => NlwInverterSignal_err_MC_D2_PT_2_IN9,
      I10 => NlwInverterSignal_err_MC_D2_PT_2_IN10,
      I11 => NlwInverterSignal_err_MC_D2_PT_2_IN11,
      I12 => NlwInverterSignal_err_MC_D2_PT_2_IN12,
      I13 => NlwInverterSignal_err_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_2_IN15,
      O => err_MC_D2_PT_2_316
    );
  err_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_err_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_err_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_err_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_err_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_err_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_err_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_err_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_err_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_err_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_err_MC_D2_PT_3_IN15,
      O => err_MC_D2_PT_3_317
    );
  err_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      O => err_MC_D2_313
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_67,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_70,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_71,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_75,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_76,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_down_cs_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D_78,
      O => NlwBufferSignal_cnt_1_down_cs_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_down_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_down_cs_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D1_79,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_MC_D2_80,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_down_cs_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_5,
      O => NlwBufferSignal_cnt_1_down_cs_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_up_cs_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D_82,
      O => NlwBufferSignal_cnt_1_up_cs_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_up_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_up_cs_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D1_83,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_MC_D2_84,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_up_cs_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_7,
      O => NlwBufferSignal_cnt_1_up_cs_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D_86,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D1_87,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_88,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D1_IN2 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_0_II_UIM_11,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_0_89,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_1_90,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_0_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_0_MC_D2_PT_2_91,
      O => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_MC_D_93,
      O => NlwBufferSignal_cnt_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_94,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_95,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_165_97,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_10_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_0_98,
      O => NlwBufferSignal_cnt_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_1_101,
      O => NlwBufferSignal_cnt_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_2_109,
      O => NlwBufferSignal_cnt_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_10_MC_D2_PT_3_110,
      O => NlwBufferSignal_cnt_10_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D_112,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_tsimcreated_xor_Q_113,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D1_114,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_115,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_1_II_UIM_13,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_1_II_UIM_13,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_0_116,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_1_117,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_2_118,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_3_119,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_1_MC_D2_PT_4_120,
      O => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D_122,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_tsimcreated_xor_Q_123,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D1_124,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_125,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_3_II_UIM_15,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_3_II_UIM_15,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_0_126,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_1_127,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_2_128,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_3_129,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_3_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_3_MC_D2_PT_4_130,
      O => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D_132,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_tsimcreated_xor_Q_133,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D1_134,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_135,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_2_II_UIM_17,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_2_II_UIM_17,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_0_136,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_1_137,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_2_138,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_3_139,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_2_MC_D2_PT_4_140,
      O => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D_142,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_tsimcreated_xor_Q_143,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D1_144,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_145,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_5_II_UIM_19,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_5_II_UIM_19,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_0_146,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_1_147,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_2_148,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_3_149,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_5_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_5_MC_D2_PT_4_150,
      O => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D_152,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_tsimcreated_xor_Q_153,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D1_154,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_155,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_4_II_UIM_21,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_4_II_UIM_21,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_0_156,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_1_157,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_2_158,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_3_159,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_4_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_4_MC_D2_PT_4_160,
      O => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D_162,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_tsimcreated_xor_Q_163,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D1_164,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_165,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_7_II_UIM_23,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_7_II_UIM_23,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_0_166,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_1_167,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_2_168,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_3_169,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_7_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_7_MC_D2_PT_4_170,
      O => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D_172,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_tsimcreated_xor_Q_173,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D1_174,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_175,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_6_II_UIM_25,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_6_II_UIM_25,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_0_176,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_1_177,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_2_178,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_3_179,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_6_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_6_MC_D2_PT_4_180,
      O => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D_182,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_tsimcreated_xor_Q_183,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D1_184,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_185,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_9_II_UIM_27,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_9_II_UIM_27,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_0_186,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_1_187,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_2_188,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_3_189,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_9_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_9_MC_D2_PT_4_190,
      O => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D_192,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_tsimcreated_xor_Q_193,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D1_194,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_195,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_8_II_UIM_29,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_8_II_UIM_29,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_0_196,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_1_197,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_2_198,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_3_199,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_8_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_8_MC_D2_PT_4_200,
      O => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D_202,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_tsimcreated_xor_Q_203,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D1_204,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_205,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_10_II_UIM_31,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_10_II_UIM_31,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_0_206,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_1_207,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_2_208,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_3_209,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_10_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_10_MC_D2_PT_4_210,
      O => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_IN4
    );
  NlwBufferBlock_N_PZ_165_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_165_MC_D1_213,
      O => NlwBufferSignal_N_PZ_165_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_165_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_165_MC_D2_214,
      O => NlwBufferSignal_N_PZ_165_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_N_PZ_165_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_N_PZ_165_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_165_MC_D2_PT_0_215,
      O => NlwBufferSignal_N_PZ_165_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_165_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_165_MC_D2_PT_1_216,
      O => NlwBufferSignal_N_PZ_165_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_165_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_165_MC_D2_PT_2_217,
      O => NlwBufferSignal_N_PZ_165_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_MC_D_219,
      O => NlwBufferSignal_cnt_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_220,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_221,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_165_97,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_11_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_0_223,
      O => NlwBufferSignal_cnt_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_1_224,
      O => NlwBufferSignal_cnt_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_2_225,
      O => NlwBufferSignal_cnt_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_3_226,
      O => NlwBufferSignal_cnt_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_11_MC_D2_PT_4_227,
      O => NlwBufferSignal_cnt_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D_229,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_Q,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_tsimcreated_xor_Q_230,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D1_231,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_232,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => nLd_II_UIM_9,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_0_233,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_1_234,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_2_235,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_3_236,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_valLd_cs_11_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs_11_MC_D2_PT_4_237,
      O => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_D_239,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_240,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_241,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_1_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_242,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_243,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_D_245,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_246,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_247,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_248,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_249,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_250,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_MC_D_252,
      O => NlwBufferSignal_cnt_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_253,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_254,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_0_255,
      O => NlwBufferSignal_cnt_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_1_256,
      O => NlwBufferSignal_cnt_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_2_257,
      O => NlwBufferSignal_cnt_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_MC_D_259,
      O => NlwBufferSignal_cnt_4_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_260,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_261,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_4_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_4_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_0_262,
      O => NlwBufferSignal_cnt_4_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_PT_1_263,
      O => NlwBufferSignal_cnt_4_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_MC_D_265,
      O => NlwBufferSignal_cnt_5_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_266,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_267,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_cnt_5_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_cnt_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_0_268,
      O => NlwBufferSignal_cnt_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_1_269,
      O => NlwBufferSignal_cnt_5_MC_D2_IN1
    );
  NlwBufferBlock_cnt_5_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_5_MC_D2_PT_2_270,
      O => NlwBufferSignal_cnt_5_MC_D2_IN2
    );
  NlwBufferBlock_cnt_6_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_MC_D_272,
      O => NlwBufferSignal_cnt_6_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_273,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_274,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_0_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_6_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_6_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_0_275,
      O => NlwBufferSignal_cnt_6_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_PT_1_276,
      O => NlwBufferSignal_cnt_6_MC_D2_IN1
    );
  NlwBufferBlock_cnt_7_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_MC_D_278,
      O => NlwBufferSignal_cnt_7_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_279,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_280,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN8
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN9
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN10
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN11
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN12
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN13
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN14
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_0_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN15
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_7_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_0_281,
      O => NlwBufferSignal_cnt_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_PT_1_282,
      O => NlwBufferSignal_cnt_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_MC_D_284,
      O => NlwBufferSignal_cnt_8_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_285,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_286,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_8_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_0_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN7
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN8
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN9
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN10
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN11 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN11
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN12 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN12
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN13
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN14
    );
  NlwBufferBlock_cnt_8_MC_D2_PT_1_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN15
    );
  NlwBufferBlock_cnt_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_0_287,
      O => NlwBufferSignal_cnt_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_PT_1_288,
      O => NlwBufferSignal_cnt_8_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_MC_D_290,
      O => NlwBufferSignal_cnt_9_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_291,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_292,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_165_97,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_cnt_9_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_cnt_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_0_293,
      O => NlwBufferSignal_cnt_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_1_294,
      O => NlwBufferSignal_cnt_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_2_295,
      O => NlwBufferSignal_cnt_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_9_MC_D2_PT_3_296,
      O => NlwBufferSignal_cnt_9_MC_D2_IN3
    );
  NlwBufferBlock_did_0_MC_D_IN0 : X_BUF
    port map (
      I => did_0_MC_D1_299,
      O => NlwBufferSignal_did_0_MC_D_IN0
    );
  NlwBufferBlock_did_0_MC_D_IN1 : X_BUF
    port map (
      I => did_0_MC_D2_300,
      O => NlwBufferSignal_did_0_MC_D_IN1
    );
  NlwBufferBlock_did_1_MC_D_IN0 : X_BUF
    port map (
      I => did_1_MC_D1_303,
      O => NlwBufferSignal_did_1_MC_D_IN0
    );
  NlwBufferBlock_did_1_MC_D_IN1 : X_BUF
    port map (
      I => did_1_MC_D2_304,
      O => NlwBufferSignal_did_1_MC_D_IN1
    );
  NlwBufferBlock_did_2_MC_D_IN0 : X_BUF
    port map (
      I => did_2_MC_D1_307,
      O => NlwBufferSignal_did_2_MC_D_IN0
    );
  NlwBufferBlock_did_2_MC_D_IN1 : X_BUF
    port map (
      I => did_2_MC_D2_308,
      O => NlwBufferSignal_did_2_MC_D_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_311,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_312,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_313,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => err_MC_UIM_310,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_1_down_cs_72,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_1_up_cs_73,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_1_valLd_cs(0),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_1_valLd_cs(10),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_1_valLd_cs(9),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => cnt_1_valLd_cs(8),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_1_valLd_cs(1),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_1_valLd_cs(3),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => cnt_1_valLd_cs(2),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => cnt_1_valLd_cs(5),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => cnt_1_valLd_cs(4),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => cnt_1_valLd_cs(7),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => cnt_1_valLd_cs(6),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => cnt_1_valLd_cs(11),
      O => NlwBufferSignal_err_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_69,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_314,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_315,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_316,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_317,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_0_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN9,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN9
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN10,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN10
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN11,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN11
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN12,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN12
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_2_IN13 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_2_IN13,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_2_IN13
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_10_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_10_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_1_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_3_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_2_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_5_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_4_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_7_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_6_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN11
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_3_IN12
    );
  NlwInverterBlock_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_9_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_3_IN11
    );
  NlwInverterBlock_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_8_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN11
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN12
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_3_IN13
    );
  NlwInverterBlock_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_10_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN7 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN7,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN7
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN8 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN8,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN8
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN9 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN9,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN9
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_1_IN10 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_1_IN10,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_1_IN10
    );
  NlwInverterBlock_N_PZ_165_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_165_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_N_PZ_165_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN11,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN11
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN12,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN12
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN13 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN13,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN13
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_3_IN14 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_3_IN14,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_3_IN14
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_11_MC_D2_PT_4_IN14 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_MC_D2_PT_4_IN14,
      O => NlwInverterSignal_cnt_11_MC_D2_PT_4_IN14
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN6
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN9
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN10
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN11
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN12
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN13
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_3_IN14
    );
  NlwInverterBlock_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_1_valLd_cs_11_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_4_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_4_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN6,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_1_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_1_IN8,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_1_IN8
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_cnt_5_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_cnt_5_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN7,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN7
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_0_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_0_IN8,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_0_IN8
    );
  NlwInverterBlock_cnt_6_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_6_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN7,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN7
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN8,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN8
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_0_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_0_IN9,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_0_IN9
    );
  NlwInverterBlock_cnt_7_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_7_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_7_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN5,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN5
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN6,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN6
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN7,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN7
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN8,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN8
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN9,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN9
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_0_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_0_IN10,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_0_IN10
    );
  NlwInverterBlock_cnt_8_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_8_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN9 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN9,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN9
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN10 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN10,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN10
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN11 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN11,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN11
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_2_IN12 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_2_IN12,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_2_IN12
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_9_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_9_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_9_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_did_0_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_did_0_MC_D_IN0,
      O => NlwInverterSignal_did_0_MC_D_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN9 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN9,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN9
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN10 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN10,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN10
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN11 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN11,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN11
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN12 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN12,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN12
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN13 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN13,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN13
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN1
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure_atil;

