
library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;


architecture arop of counter is
signal cnt_ns : std_logic_vector(11 downto 0);
signal valLd_ns : std_logic_vector(11 downto 0);
signal err_ns : std_logic;
signal up_ns : std_logic;
signal down_ns : std_logic;

signal cnt_cs : std_logic_vector(11 downto 0):= (others=>'0');
signal valLd_cs : std_logic_vector(11 downto 0):= (others=>'0');
signal err_cs : std_logic := '0';
signal up_cs : std_logic := '0';
signal down_cs : std_logic := '0';

begin

up_ns <= up;
down_ns <= down;
valLd_ns <= valLd;

reg:
    process (clk) is
    begin
        if clk = '1' and clk'event then
            if nres = '0'   then
                cnt_cs <= (others=>'0');
                valLd_cs <= (others=>'0');
                err_cs <= '0';
                up_cs <= '0';
                down_cs <= '0';
            else
                cnt_cs <= cnt_ns;
                if nLd = '0' then
                	valLd_cs <= valLd_ns;
                else
                	valLd_cs <= cnt_ns;
                end if;
                err_cs <= err_ns;
                up_cs <= up_ns;
                down_cs <= down_ns;
            end if;
        end if;
    end process reg;



    counter:
	process(valLd_cs, up_cs, down_cs, err_cs) is
    	variable cnt_v : std_logic_vector(11 downto 0);
        variable err_flag_v : std_logic;
	begin
    	cnt_v := valLd_cs;
        err_flag_v := err_cs;

    	if up_cs = '1' and down_cs = '0' then
    		cnt_v := cnt_v + 1;
            if valLd_cs > cnt_v then
                err_flag_v := '1';
            end if;
    	elsif up_cs = '0' and down_cs = '1' then
    		cnt_v := cnt_v + "111111111111";
            if valLd_cs < cnt_v then
                err_flag_v := '1';
            end if;
        elsif up_cs = '1' and down_cs = '1' then
            err_flag_v := '1';
    	end if;

        err_ns <= err_flag_v;
    	cnt_ns <= cnt_v;
	end process counter;

cnt <= cnt_cs;
err <= err_cs;
did <= "100";


end architecture arop;