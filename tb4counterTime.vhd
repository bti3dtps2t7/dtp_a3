
library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic

entity tb4counterTime is
end entity tb4counterTime;


architecture beh of tb4counterTime is
	signal ValLd_s : std_logic_vector(11 downto 0) := (others=>'0');
	signal up_s : std_logic  := '0';
	signal down_s : std_logic := '0';
	signal nLd_s : std_logic := '1';
	signal clk_s : std_logic;
	signal nres_s : std_logic;
	signal did_atop_s : std_logic_vector( 2 downto 0 ) := (others=>'0');
    signal err_atop_s : std_logic := '0';
   	signal cnt_atop_s : std_logic_vector( 11 downto 0 ) := (others=>'0');
   	signal did_atgl_s : std_logic_vector( 2 downto 0 ) := (others=>'0');
    signal err_atgl_s : std_logic := '0';
   	signal cnt_atgl_s : std_logic_vector( 11 downto 0 ) := (others=>'0');
   	signal did_atil_s : std_logic_vector( 2 downto 0 ) := (others=>'0');
    signal err_atil_s : std_logic := '0';
   	signal cnt_atil_s : std_logic_vector( 11 downto 0 ) := (others=>'0');
   	signal ok_s : std_logic;


    component sg4counter is
        port(
        	sg_ValLd : out std_logic_vector(11 downto 0);
        	sg_up : out std_logic;
        	sg_down : out std_logic;
        	sg_nLd : out std_logic;
        	sg_clk : out std_logic;
        	sg_nres : out std_logic
        );--]port
    end component sg4counter;
    for all : sg4counter use entity work.sg4counter( beh );

    component rc4counter is
        port(
            clk : in std_logic;
            err_arop : in std_logic;
            cnt_arop : in std_logic_vector(11 downto 0);
            err_argl : in std_logic;
            cnt_argl : in std_logic_vector(11 downto 0);
            err_aril : in std_logic;
            cnt_aril : in std_logic_vector(11 downto 0);
            ok : out std_logic
            );
    end component rc4counter;
    for all : rc4counter use entity work.rc4counter( beh );

    component DUT is
    port ( 
    	did   : out std_logic_vector(  2 downto 0 );  --Dut IDentifier
    	err   : out std_logic;                        --ERRor : invalid counter value
    	cnt   : out std_logic_vector( 11 downto 0 );  --CouNT
    	valLd : in  std_logic_vector( 11 downto 0 );  --init VALue in case of LoaD
    	nLd   : in  std_logic;                        --Not LoaD; low actve LoaD
    	up    : in  std_logic;                        --UP count command
    	down  : in  std_logic;                        --DOWN count command
    	clk   : in  std_logic;                        --CLocK
    	nres  : in  std_logic                         --Not RESet ; low active synchronous reset
  	);--]port
  	end component DUT;
  	for cnt_1 : DUT use entity work.DUT( Structure_atop );
    for cnt_2 : DUT use entity work.DUT( Structure_atgl);
    for cnt_3 : DUT use entity work.DUT( Structure_atil);
begin



sg_i : sg4counter
    port map (
       	sg_ValLd => ValLd_s,
    	sg_up => up_s,
    	sg_down => down_s,
    	sg_nLd => nLd_s,
    	sg_clk => clk_s,
    	sg_nres => nres_s
        )--]port
;--]sg_i

rc_i : rc4counter
    port map(
        clk => clk_s,
        err_arop => err_atop_s,
        cnt_arop => cnt_atop_s,
        err_argl => err_atgl_s,
        cnt_argl => cnt_atgl_s,
        err_aril => err_atil_s,
        cnt_aril => cnt_atil_s,
        ok => ok_s
    )
;

cnt_1 : DUT
    port map (
        valLd => ValLd_s,
        nLd => nLd_s,
        up => up_s,
        down => down_s,
        clk => clk_s,
        nres => nres_s,
        did => did_atop_s,
        err => err_atop_s,
        cnt => cnt_atop_s
    )--]port
;--]ha_i1

cnt_2 : DUT
    port map (
        valLd => ValLd_s,
        nLd => nLd_s,
        up => up_s,
        down => down_s,
        clk => clk_s,
        nres => nres_s,
        did => did_atgl_s,
        err => err_atgl_s,
        cnt => cnt_atgl_s
    )--]port
;--]ha_i1

cnt_3 : DUT
    port map (
        valLd => ValLd_s,
        nLd => nLd_s,
        up => up_s,
        down => down_s,
        clk => clk_s,
        nres => nres_s,
        did => did_atil_s,
        err => err_atil_s,
        cnt => cnt_atil_s
    )--]port
;--]ha_i1




end architecture beh;